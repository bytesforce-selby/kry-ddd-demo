package com.kry.product.query.dataobjects;

import lombok.Data;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Data
public class CategoryDO {

    private String categoryId;
    private String categoryName;

}
