package com.kry.product.query.dataobjects;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Accessors(chain = true)
@Data
public class ProductDO {

    private String productId;
    private String productName;
    private String briefDesc;
    private String authorName;
    private String authorProfile;
    private String publishingName;
    private Date publishedDate;
    private BigDecimal price;
    private String medias;

    private List<String> mediaList;

}
