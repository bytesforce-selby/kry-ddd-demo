package com.kry.product.query.dao;

import com.kry.product.query.dataobjects.CategoryDO;
import com.kry.product.query.dataobjects.ProductDO;

import java.util.List;

public interface ProductDAO {

    List<CategoryDO> findAllAvailableCategories();

    List<ProductDO> findSellingProductsByName(String productName);

    List<ProductDO> findSellingProductsByCategory(String categoryId);

}
