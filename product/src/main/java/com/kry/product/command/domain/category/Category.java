package com.kry.product.command.domain.category;

import com.kry.domain.Aggregate;
import com.kry.product.command.application.valueobjects.CategoryChangeVO;
import lombok.Builder;
import lombok.Getter;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.UUID;

@Builder
@Getter
public class Category extends Aggregate {

    private String id;
    private String name;
    private String description;
    private CategoryStatus status;


    public void create() {
        this.id = UUID.randomUUID().toString();
        this.status = CategoryStatus.ENABLED;
    }

    public void changeInfo(CategoryChangeVO changeVO) {
        if (!StringUtils.isEmpty(changeVO.getName())) {
            this.name = changeVO.getName();
        }
        if (!StringUtils.isEmpty(changeVO.getDescription())) {
            this.description = changeVO.getDescription();
        }
    }

    public void enable() {
        this.status = CategoryStatus.ENABLED;
    }

    public void disable() {
        this.status = CategoryStatus.DISABLED;
    }

    @Override
    public Serializable getAggregateId() {
        return id;
    }
}
