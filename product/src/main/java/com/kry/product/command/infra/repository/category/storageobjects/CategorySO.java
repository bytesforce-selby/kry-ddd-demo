package com.kry.product.command.infra.repository.category.storageobjects;

import com.kry.infra.repository.BaseSO;
import com.kry.infra.repository.mybatis.annotation.StorageMapper;
import com.kry.product.command.infra.repository.category.mybatis.mapper.CategoryMapper;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@StorageMapper(CategoryMapper.class)
@Accessors(chain = true)
@Data
public class CategorySO extends BaseSO {

    private String categoryId;
    private String categoryName;
    private String description;
    private int status;

    @Override
    public Serializable getPrimaryKeyValue() {
        return categoryId;
    }
}
