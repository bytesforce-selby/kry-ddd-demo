package com.kry.product.command.infra.repository.product.mybatis.mapper;

import com.kry.product.command.infra.repository.product.storageobjects.ProductCategorySO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

@Mapper
public interface ProductCategoryMapper {

    @Insert({
            "insert into product_category (product_id, ",
            "category_id)",
            "values (#{productId,jdbcType=VARCHAR}, ",
            "#{categoryId,jdbcType=VARCHAR})"
    })
    int insert(ProductCategorySO record);

    @Select({
            "select",
            "product_id, category_id ",
            "from product_category",
            "where product_id = #{productId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
            @Result(column="category_id", property="categoryId", jdbcType=JdbcType.VARCHAR)
    })
    List<ProductCategorySO> selectByProductId(String productId);

    @Delete({
            "delete from product_category",
            "where product_id = #{productId,jdbcType=VARCHAR}"
    })
    int deleteByProductId(String productId);

}
