package com.kry.product.command.infra.repository.product.storageobjects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

@AllArgsConstructor
@Accessors(chain = true)
@Data
public class ProductCategorySO {

    private String productId;
    private String categoryId;

}
