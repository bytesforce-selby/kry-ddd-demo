package com.kry.product.command.application.valueobjects;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Accessors(chain = true)
@Data
public class CategoryCreationVO {

    @NotNull(message = "name must be provided.")
    private String name;
    private String description;

}
