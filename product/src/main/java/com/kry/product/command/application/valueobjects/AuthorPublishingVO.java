package com.kry.product.command.application.valueobjects;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Accessors(chain = true)
@Data
public class AuthorPublishingVO {

    private String authorName;
    private String authorProfile;
    private String publishingName;
    private Date publishedDate;

}
