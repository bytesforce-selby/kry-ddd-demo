package com.kry.product.command.domain.product;

import com.kry.domain.Repository;

public interface ProductRepository extends Repository<Product> {
}
