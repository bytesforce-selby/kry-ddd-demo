package com.kry.product.command.domain.product;

import com.kry.domain.BaseDomainEvent;
import lombok.Getter;

import java.util.List;

@Getter
public final class ProductCreatedEvent extends BaseDomainEvent {

    private String productId;
    private String productName;

    public ProductCreatedEvent(String productId, String productName) {
        this.productId = productId;
        this.productName = productName;
    }
}
