package com.kry.product.command.application;

import com.google.common.collect.Lists;
import com.kry.product.command.application.valueobjects.*;
import com.kry.product.command.domain.category.Category;
import com.kry.product.command.domain.category.CategoryRepository;
import com.kry.product.command.domain.product.*;
import com.kry.utils.KryAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

@Transactional
@Service
public class ProductApplicationService {

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ProductRepository productRepository;


    public void createCategory(CategoryCreationVO categoryVO) {
        KryAssert.notNull(categoryVO, "categoryVO must be provided.");

        Category category = Category.builder().name(categoryVO.getName())
                .description(categoryVO.getDescription()).build();
        category.create();

        categoryRepository.save(category);

    }

    public CategoryVO loadCategory(String categoryId) {
        KryAssert.notEmpty(categoryId, "categoryId must be provided.");

        Category category = categoryRepository.load(categoryId);

        CategoryVO categoryVO = new CategoryVO();
        categoryVO.setCategoryId(category.getId()).setName(category.getName())
                .setDescription(category.getDescription()).setStatus(category.getStatus().toString());

        return categoryVO;
    }

    public void changeCategoryInfo(String categoryId, CategoryChangeVO categoryVO) {
        KryAssert.notNull(categoryVO, "categoryVO must be provided.");

        Category category = categoryRepository.load(categoryId);
        category.changeInfo(categoryVO);

        categoryRepository.save(category);
    }

    public void enableCategory(String categoryId) {
        KryAssert.notEmpty(categoryId, "categoryId must be provided.");

        Category category = categoryRepository.load(categoryId);
        category.enable();

        categoryRepository.save(category);
    }

    public void disableCategory(String categoryId) {
        KryAssert.notEmpty(categoryId, "categoryId must be provided.");

        Category category = categoryRepository.load(categoryId);
        category.disable();

        categoryRepository.save(category);
    }

    public void createProduct(ProductCreationVO productVO) {
        KryAssert.notNull(productVO, "productVO must be provided.");

        Product product = Product.builder().name(productVO.getName())
                .briefDesc(productVO.getBriefDesc())
                .price(productVO.getPrice())
                .author(Author.builder().profile(productVO.getAuthorProfile())
                        .name(productVO.getName()).build())
                .publishing(Publishing.builder().date(productVO.getPublishedDate())
                        .company(productVO.getPublishingName()).build())
                .build();
        product.addMedias(productVO.getMediaIds());
        product.assignToCategories(productVO.getCategoryIds());
        product.create();

        productRepository.save(product);
    }

    public void assignCategoriesTo(String productId, List<String> categoryIds) {
        if (CollectionUtils.isEmpty(categoryIds)) {
            return;
        }
        KryAssert.notEmpty(productId, "productId must be provided.");

        Product product = productRepository.load(productId);
        product.assignToCategories(categoryIds);

        productRepository.save(product);
    }

    public void updateMedias(String productId, List<String> mediaIds) {
        if (CollectionUtils.isEmpty(mediaIds)) {
            return;
        }
        KryAssert.notEmpty(productId, "productId must be provided.");

        Product product = productRepository.load(productId);
        product.maintainMedia(mediaIds);

        productRepository.save(product);
    }

    public void changeAuthorAndPublishing(String productId, AuthorPublishingVO authorPublishingVO) {
        KryAssert.notNull(authorPublishingVO, "authorPublishingVO must be provided.");

        Product product = productRepository.load(productId);
        product.changeAuthorAndPublishingInfo(authorPublishingVO);

        productRepository.save(product);
    }

    public void changePrice(String productId, BigDecimal price) {
        KryAssert.notEmpty(productId, "productId must be provided.");
        KryAssert.notNull(price, "price must be provided.");

        Product product = productRepository.load(productId);
        product.changePrice(price);

        productRepository.save(product);
    }

    public void changeName(String productId, String productName) {
        KryAssert.notEmpty(productId, "productId must be provided.");
        KryAssert.notEmpty(productName, "productName must be provided.");

        Product product = productRepository.load(productId);
        product.changeName(productName);

        productRepository.save(product);
    }

    public void changeStateOfProduct(String productId, boolean selling) {
        KryAssert.notEmpty(productId, "productId must be provided.");

        Product product = productRepository.load(productId);
        if (selling) {
            product.sell();
        } else {
            product.unsell();
        }

        productRepository.save(product);
    }


    @Transactional(readOnly = true)
    public ProductVO loadProduct(String productId) {
        KryAssert.notEmpty(productId, "productId must be provided.");

        Product product = productRepository.load(productId);

        ProductVO productVO = new ProductVO();
        productVO.setProductId(product.getId()).setName(product.getName())
                .setPrice(product.getPrice()).setBriefDesc(product.getBriefDesc())
                .setStatus(product.getStatus().toString())
                .setAuthorName(product.getAuthor().getName()).setAuthorProfile(product.getAuthor().getProfile())
                .setPublishingName(product.getPublishing().getCompany()).setPublishedDate(product.getPublishing().getDate());
        if (!CollectionUtils.isEmpty(product.getMedias())) {
            List<String> medias = Lists.newArrayList();
            for (MediaId id: product.getMedias()) {
                medias.add(id.getFileId());
            }
            productVO.setMediaIds(medias);
        }
        if (!CollectionUtils.isEmpty(product.getCategories())) {
            List<String> medias = Lists.newArrayList();
            for (CategoryId id: product.getCategories()) {
                medias.add(id.getId());
            }
            productVO.setCategoryIds(medias);
        }

        return productVO;

    }

}
