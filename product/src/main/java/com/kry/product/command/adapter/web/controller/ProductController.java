package com.kry.product.command.adapter.web.controller;


import com.kry.product.command.application.ProductApplicationService;
import com.kry.product.command.application.valueobjects.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@Api(description = "Catalog management", tags = "100 - Catalog APIs")
@RestController
@RequestMapping("/catalog")
public class ProductController {

    @Autowired
    private ProductApplicationService productService;

    @ApiOperation(value = "Create a category", notes = "Create a category")
    @ResponseBody
    @PostMapping(value = "/category", produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void createCategory(@RequestBody CategoryCreationVO category) {
        productService.createCategory(category);
    }

    @ApiOperation(value = "Change category info", notes = "Change category info")
    @ResponseBody
    @PatchMapping(value = "/category/{categoryId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void changeCategoryInfo(@PathVariable String categoryId, @RequestBody CategoryChangeVO category) {
        productService.changeCategoryInfo(categoryId, category);
    }

    @ApiOperation(value = "Enable category", notes = "Enable category")
    @ResponseBody
    @PutMapping(value = "/category/{categoryId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void enableCategory(@PathVariable String categoryId) {
        productService.enableCategory(categoryId);
    }

    @ApiOperation(value = "Disable category", notes = "Disable category")
    @ResponseBody
    @DeleteMapping(value = "/category/{categoryId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void disableCategory(@PathVariable String categoryId) {
        productService.disableCategory(categoryId);
    }


    @ApiOperation(value = "Create a product", notes = "Create a product")
    @ResponseBody
    @PostMapping(value = "/product", produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void createProduct(@RequestBody ProductCreationVO product) {
        productService.createProduct(product);
    }

    @ApiOperation(value = "Reassign categories to product", notes = "Reassign categories to product")
    @ResponseBody
    @PatchMapping(value = "/product/{productId}/categories", produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void reassignCategories(@PathVariable String productId, @RequestBody List<String> catetories) {
        productService.assignCategoriesTo(productId, catetories);
    }

    @ApiOperation(value = "Maintain medias of product", notes = "Maintain medias of product")
    @ResponseBody
    @PatchMapping(value = "/product/{productId}/medias", produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void maintainMedia(@PathVariable String productId, @RequestBody List<String> medias) {
        productService.updateMedias(productId, medias);
    }

    @ApiOperation(value = "Edit author and publishing info of product", notes = "Edit author and publishing info of product")
    @ResponseBody
    @PatchMapping(value = "/product/{productId}/basic", produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void changeAuthorAndPublishing(@PathVariable String productId, @RequestBody AuthorPublishingVO authorAndPublishing) {
        productService.changeAuthorAndPublishing(productId, authorAndPublishing);
    }

    @ApiOperation(value = "Change price of product", notes = "Change price of product")
    @ResponseBody
    @PatchMapping(value = "/product/{productId}/price", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void changePrice(@PathVariable String productId, @RequestParam BigDecimal price) {
        productService.changePrice(productId, price);
    }

    @ApiOperation(value = "Change name of product", notes = "Change name of product")
    @ResponseBody
    @PatchMapping(value = "/product/{productId}/name", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void changeProductName(@PathVariable String productId, @RequestParam String productName) {
        productService.changeName(productId, productName);
    }

    @ApiOperation(value = "Stop selling for product", notes = "Stop selling for product")
    @ResponseBody
    @PatchMapping(value = "/product/{productId}/non-selling", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void stopSelling(@PathVariable String productId) {
        productService.changeStateOfProduct(productId, false);
    }

    @ApiOperation(value = "Start selling for product", notes = "Start selling for product")
    @ResponseBody
    @PatchMapping(value = "/product/{productId}/selling", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void startSelling(@PathVariable String productId) {
        productService.changeStateOfProduct(productId, true);
    }

    @ApiOperation(value = "Load product info", notes = "Load product info")
    @ResponseBody
    @GetMapping(value = "/product/{productId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ProductVO loadProduct(@PathVariable String productId) {
        return productService.loadProduct(productId);
    }

    @ApiOperation(value = "Load category info", notes = "Load category info")
    @ResponseBody
    @GetMapping(value = "/category/{categoryId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public CategoryVO loadCategory(@PathVariable String categoryId) {
        return productService.loadCategory(categoryId);
    }

}
