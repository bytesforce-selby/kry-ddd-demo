package com.kry.product.command.infra.repository.product.storageobjects;

import com.kry.infra.repository.BaseSO;
import com.kry.infra.repository.mybatis.annotation.StorageMapper;
import com.kry.product.command.infra.repository.product.mybatis.mapper.ProductMapper;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@StorageMapper(ProductMapper.class)
@Accessors(chain = true)
@Data
public class ProductSO extends BaseSO {

    private String productId;
    private String productName;
    private String briefDesc;
    private BigDecimal price;

    private int status;

    private String authorName;
    private String authorProfile;

    private String publishingName;
    private Date publishedDate;

    private String medias;

    @Override
    public Serializable getPrimaryKeyValue() {
        return productId;
    }

}
