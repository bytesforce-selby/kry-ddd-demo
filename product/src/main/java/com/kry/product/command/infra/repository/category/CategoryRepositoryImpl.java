package com.kry.product.command.infra.repository.category;

import com.kry.domain.AbstractRepository;
import com.kry.domain.DomainEvent;
import com.kry.exception.AggregateNotFoundException;
import com.kry.infra.repository.DaoProvider;
import com.kry.product.command.domain.category.Category;
import com.kry.product.command.domain.category.CategoryRepository;
import com.kry.product.command.domain.category.CategoryStatus;
import com.kry.product.command.infra.repository.category.storageobjects.CategorySO;
import com.kry.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public class CategoryRepositoryImpl extends AbstractRepository<Category> implements CategoryRepository {

    @Autowired
    private DaoProvider daoProvider;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Override
    protected void saveAggregate(Category aggregate) {
        CategorySO categorySO = new CategorySO();
        categorySO.setCategoryId(aggregate.getId()).setCategoryName(aggregate.getName())
                .setDescription(aggregate.getDescription()).setStatus(aggregate.getStatus().value());
        daoProvider.save(categorySO);
    }

    @Override
    protected void publishEvent(Category aggregate) {
        DomainEvent event = getEvent(aggregate);
        if (event != null) {
            kafkaTemplate.send(getDefaultTopicName(event), aggregate.getEntityId().toString(), JsonUtils.toJson(event));
        }
    }

    @Override
    public Category load(Serializable aggregateId) {
        CategorySO categorySO = daoProvider.loadOne(CategorySO.class, aggregateId);
        if (categorySO == null) {
            throw new AggregateNotFoundException();
        }

        Category category = Category.builder().id(categorySO.getCategoryId()).name(categorySO.getCategoryName())
                .description(categorySO.getDescription()).status(CategoryStatus.from(categorySO.getStatus()))
                .build();

        return category;
    }
}
