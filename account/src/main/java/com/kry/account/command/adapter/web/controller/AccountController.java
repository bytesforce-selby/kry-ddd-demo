package com.kry.account.command.adapter.web.controller;

import com.kry.account.command.adapter.web.controller.dataobjects.LoginDO;
import com.kry.account.command.application.service.AccountApplicationService;
import com.kry.account.command.application.valueobjects.AccountVO;
import com.kry.account.command.application.valueobjects.LoginResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Api(description = "Include account registration, status management and login.", tags = "100 - Account APIs")
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountApplicationService accountService;

    @ApiOperation(value = "Account registration", notes = "Account registration")
    @PostMapping
    @ResponseBody
    public void register(@RequestBody AccountVO account) {
        accountService.register(account);
    }

    @ApiOperation(value = "Account login", notes = "Account login")
    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public LoginResultVO login(@RequestBody LoginDO loginInfo) {
        return accountService.login(loginInfo.getAccountName(), loginInfo.getPassword());
    }

    @ApiOperation(value = "Account activation", notes = "Activate an account")
    @PatchMapping(value = "/{accountId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public void activate(@PathVariable String accountId) {
        accountService.activate(accountId);
    }

    @ApiOperation(value = "Account disablement", notes = "Disable an account")
    @PutMapping(value = "/{accountId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public void disable(@PathVariable String accountId) {
        accountService.disable(accountId);
    }

}
