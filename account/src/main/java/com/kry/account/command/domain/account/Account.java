package com.kry.account.command.domain.account;

import com.kry.IUser;
import com.kry.account.command.exception.DuplicateAccountException;
import com.kry.account.command.exception.LoginException;
import com.kry.domain.Aggregate;
import com.kry.utils.EncryptUtils;
import lombok.Builder;
import lombok.Getter;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.UUID;

@Builder
@Getter
public class Account extends Aggregate implements IUser {

    private String id;
    private String accountName;
    private String password;
    private AccountStatus status;
    private LoginInfo loginInfo;
    private LockedInfo lockedInfo;

    public void register() {
        Account account = getBean(AccountRepository.class).loadByAccountName(accountName);
        if (account != null) {
            throw new DuplicateAccountException(String.format("%s has been registered.", accountName));
        }

        this.status = AccountStatus.ACTIVE;
        this.password = EncryptUtils.encryptWithAES256(password);
        this.id = UUID.randomUUID().toString();

        applyEvent(AccountRegisteredEvent.builder().accountId(id).accountName(accountName).build());
    }

    public boolean login(String password) {
        checkStatus();
        boolean result = true;
        if (!verifyPassword(password)) {
            result = false;
            if (loginInfo == null) {
                loginInfo = LoginInfo.newWithLoginFailed();
            } else {
                if (loginInfo.isOverMaxTimesOfLoginFailed()) {
                    lockedInfo = LockedInfo.lock();
                    status = AccountStatus.LOCKED;
                }
                loginInfo = loginInfo.increaseTimesOfLoginFailed();
            }
        } else {
            loginInfo = LoginInfo.newWithLoginSuccessfully();
        }
        return result;
    }

    private void checkStatus() {
        if (status == AccountStatus.LOCKED) {
            throw LoginException.newWithLocked();
        }
        if (status == AccountStatus.DISABLED) {
            throw LoginException.newWithDisabled();
        }
        if (status == AccountStatus.INACTIVE) {
            throw LoginException.newWithInactive();
        }
    }

    private boolean verifyPassword(String password) {
        if (StringUtils.isEmpty(password)) {
            return false;
        }
        return EncryptUtils.encryptWithAES256(password).equals(this.password);
    }

    public void activate() {
        this.status = AccountStatus.ACTIVE;
        applyEvent(AccountActivatedEvent.builder().accountId(id).accountName(accountName).build());
    }

    public void disable() {
        this.status = AccountStatus.DISABLED;
        applyEvent(AccountDisabledEvent.builder().accountId(id).accountName(accountName).build());
    }


    @Override
    public Serializable getUserId() {
        return id;
    }

    @Override
    public String getUserName() {
        return accountName;
    }

    @Override
    public Serializable getAggregateId() {
        return id;
    }
}
