package com.kry.account.command.domain.account;

import com.kry.domain.BaseDomainEvent;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class AccountRegisteredEvent extends BaseDomainEvent {

    private String accountId;
    private String accountName;

}
