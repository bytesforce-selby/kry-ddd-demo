package com.kry.account.command.application.valueobjects;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Accessors(chain = true)
@Setter
@Getter
public class AccountVO {
    @NotNull
    private String accountName;
    @NotNull
    private String password;

}
