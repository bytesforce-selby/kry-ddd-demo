package com.kry.account.command.domain.account;

import com.kry.domain.ValueObject;
import lombok.Builder;
import lombok.Getter;
import org.joda.time.DateTime;

import java.util.Date;

@Builder
@Getter
public class LoginInfo implements ValueObject {

    private Date loginAt;
    private int timesOfLoginFailed;

    private static final int MAX_TIMES_OF_LOGIN_FAILED = 3;

    public LoginInfo increaseTimesOfLoginFailed() {
        return LoginInfo.builder().timesOfLoginFailed(timesOfLoginFailed + 1).build();
    }

    public boolean isOverMaxTimesOfLoginFailed() {
        return timesOfLoginFailed + 1 > MAX_TIMES_OF_LOGIN_FAILED;
    }

    public static LoginInfo newWithLoginFailed() {
        return LoginInfo.builder().timesOfLoginFailed(1).build();
    }

    public static LoginInfo newWithLoginSuccessfully() {
        return LoginInfo.builder().loginAt(DateTime.now().toDate()).build();
    }

}
