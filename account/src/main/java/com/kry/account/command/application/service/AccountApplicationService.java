package com.kry.account.command.application.service;

import com.kry.account.command.application.valueobjects.AccountVO;
import com.kry.account.command.application.valueobjects.LoginResultVO;
import com.kry.account.command.domain.account.Account;
import com.kry.account.command.domain.account.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.security.InvalidParameterException;

@Transactional
@Service
public class AccountApplicationService {

    @Autowired
    private AccountRepository accountRepository;


    public LoginResultVO login(String accountName, String password) {
        if (StringUtils.isEmpty(accountName)) {
            throw new InvalidParameterException("accountName must be provided.");
        }
        if (StringUtils.isEmpty(password)) {
            throw new InvalidParameterException("password must be provided.");
        }

        LoginResultVO result = new LoginResultVO();

        Account account = accountRepository.loadByAccountName(accountName);
        if (account == null) {
            result.setLogged(false);
        } else {
            result.setLogged(account.login(password));
            result.setAccountId(account.getId()).setAccountName(account.getAccountName());
            accountRepository.save(account);
        }

        return result;
    }

    public void register(AccountVO accountVO) {
        if (accountVO == null) {
            throw new InvalidParameterException("account info cannot be empty.");
        }

        Account account = Account.builder().accountName(accountVO.getAccountName())
                .password(accountVO.getPassword()).build();
        account.register();

        accountRepository.save(account);
    }

    public void activate(String accountId) {
        if (StringUtils.isEmpty(accountId)) {
            throw new InvalidParameterException("accountId must be provided.");
        }

        Account account = accountRepository.load(accountId);
        account.activate();

        accountRepository.save(account);
    }

    public void disable(String accountId) {
        if (StringUtils.isEmpty(accountId)) {
            throw new InvalidParameterException("accountId must be provided.");
        }

        Account account = accountRepository.load(accountId);
        account.disable();

        accountRepository.save(account);
    }

}
