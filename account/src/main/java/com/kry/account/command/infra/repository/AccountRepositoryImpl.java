package com.kry.account.command.infra.repository;

import com.kry.account.command.domain.account.Account;
import com.kry.account.command.domain.account.AccountRepository;
import com.kry.account.command.domain.account.AccountStatus;
import com.kry.account.command.domain.account.LockedInfo;
import com.kry.account.command.domain.account.LoginInfo;
import com.kry.account.command.infra.repository.mybatis.mapper.AccountMapper;
import com.kry.account.command.infra.repository.storageobjects.AccountSO;
import com.kry.domain.AbstractRepository;
import com.kry.domain.DomainEvent;
import com.kry.exception.AggregateNotFoundException;
import com.kry.infra.repository.DaoProvider;
import com.kry.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public class AccountRepositoryImpl extends AbstractRepository<Account> implements AccountRepository {

    @Autowired
    private AccountMapper accountMapper;
    @Autowired
    private DaoProvider daoProvider;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public Account loadByAccountName(String accountName) {
        AccountSO accountSO = accountMapper.selectByAccountName(accountName);
        if (accountSO == null) {
            return null;
        }
        return buildAccount(accountSO);
    }

    @Override
    protected void saveAggregate(Account aggregate) {
        AccountSO accountSO = new AccountSO();
        accountSO.withAccountId(aggregate.getId()).withAccountName(aggregate.getAccountName())
                .withPassword(aggregate.getPassword())
                .withStatus(aggregate.getStatus().value());

        if (aggregate.getLockedInfo() != null) {
            accountSO.withLockedAt(aggregate.getLockedInfo().getLockedAt())
                    .withUnLockedAt(aggregate.getLockedInfo().getUnlockAt())
                    .withLockedReason(aggregate.getLockedInfo().getLockedReason());
        }

        if (aggregate.getLoginInfo() != null) {
            accountSO.withLoginAt(aggregate.getLoginInfo().getLoginAt())
                    .withTimesOfLoginFailed(aggregate.getLoginInfo().getTimesOfLoginFailed());
        }

        daoProvider.save(accountSO);

    }

    @Override
    protected void publishEvent(Account aggregate) {
        DomainEvent event = getEvent(aggregate);
        if (event != null) {
            kafkaTemplate.send(getDefaultTopicName(event), aggregate.getEntityId().toString(), JsonUtils.toJson(event));
        }
    }

    @Override
    public Account load(Serializable aggregateId) {
        AccountSO accountSO = daoProvider.loadOne(AccountSO.class, aggregateId);
        if (accountSO == null) {
            throw new AggregateNotFoundException(String.format("Aggregate cannot be found via aggregate id [%s]", aggregateId));
        }
        return buildAccount(accountSO);
    }

    private Account buildAccount(AccountSO accountSO) {
        Account account = Account.builder().accountName(accountSO.getAccountName()).id(accountSO.getAccountId())
                .password(accountSO.getPassword()).status(AccountStatus.valueOf(accountSO.getStatus()))
                .lockedInfo(LockedInfo.builder().unlockAt(accountSO.getUnLockedAt())
                        .lockedAt(accountSO.getLockedAt()).lockedReason(accountSO.getLockedReason()).build())
                .loginInfo(LoginInfo.builder().loginAt(accountSO.getLoginAt())
                        .timesOfLoginFailed(accountSO.getTimesOfLoginFailed())
                        .build())
                .build();
        return account;
    }
}
