package com.kry.account.command.domain.account;

import com.kry.exception.UnrecognizedException;

public enum AccountStatus {

    INACTIVE(0), ACTIVE(1), LOCKED(2), DISABLED(3);

    private int status;
    AccountStatus(int status) {
        this.status = status;
    }

    public static AccountStatus valueOf(int status) {
        if (status == 0) {
            return INACTIVE;
        } else if (status == 1) {
            return ACTIVE;
        } else if (status == 2) {
            return LOCKED;
        } else if (status == 3) {
            return DISABLED;
        } else {
            throw new UnrecognizedException();
        }
    }

    public Integer value() {
        return status;
    }

}
