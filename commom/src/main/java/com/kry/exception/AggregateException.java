package com.kry.exception;

public class AggregateException extends RuntimeException {

    public AggregateException(String message) {
        super(message);
    }

}
