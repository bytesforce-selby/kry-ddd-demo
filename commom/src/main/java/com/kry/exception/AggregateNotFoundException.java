package com.kry.exception;

public class AggregateNotFoundException extends RuntimeException {

    public AggregateNotFoundException() {
    }

    public AggregateNotFoundException(String aggregateId) {
        super(String.format("Aggregate not found with aggregate id [%s].", aggregateId));
    }

}
