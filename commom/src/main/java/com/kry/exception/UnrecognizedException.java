package com.kry.exception;

public class UnrecognizedException extends RuntimeException {

    public UnrecognizedException() {
    }

    public UnrecognizedException(String message) {
        super(message);
    }
    
}
