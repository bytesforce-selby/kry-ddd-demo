package com.kry.infra.repository;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * This is the root object of storage object. That means all storage objects have to inherit it.
 */
@Accessors(chain = true)
@Data
public abstract class BaseSO {

    private Date createDate;
    private String createUser;
    private Date updateDate;
    private String updateUser;
    private int ver;

    public abstract Serializable getPrimaryKeyValue();

}