package com.kry.infra.repository.mybatis;

import com.kry.exception.OptimisticLockException;
import com.kry.exception.StorageException;
import com.kry.infra.repository.mybatis.annotation.StorageMapper;
import com.kry.infra.repository.BaseSO;
import com.kry.infra.repository.DaoProvider;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class MyBatisDaoPrivider implements DaoProvider {

    @Autowired
    private SqlSessionTemplate sessionTemplate;

    @Override
    public <T extends BaseSO> Serializable save(T entity) {
        if (StringUtils.isEmpty(entity.getPrimaryKeyValue())) {
            throw new StorageException("Primary key must be provided.");
        }
        MapperInfo mapper = getMapperAnnotation(entity.getClass());
        T entityStored = sessionTemplate.selectOne(mapper.getMapperClass().concat(".").concat(mapper.getSelectOneMethodName()), entity.getPrimaryKeyValue());
        boolean update = false;
        if (entityStored != null) {
            entity.setVer(entityStored.getVer() + 1);
            entity.setCreateDate(entityStored.getCreateDate());
            entity.setCreateUser(entityStored.getCreateUser());
            update = true;
        } else {
            entity.setVer(1);
            entity.setCreateDate(DateTime.now().toDate());
            // TODO
            //entity.setCreateUser(AppContext.currentUser().getUserId());
            entity.setCreateUser("Should be login user");
        }
        entity.setUpdateDate(DateTime.now().toDate());
        // TODO
        //entity.setUpdateUser(AppContext.currentUser().getUserId());
        entity.setUpdateUser("Should be login user");

        if (update) {
            Map<String, Object> parameter = new HashMap<>();
            parameter.put(mapper.getAliasName(), entity);
            parameter.put("ver", entityStored.getVer());
            int cnt = sessionTemplate.update(mapper.getMapperClass().concat(".").concat(mapper.getUpdateMethodName()), parameter);
            if (cnt != 1) {
                throw new OptimisticLockException(String.format("This entity %s with version %s has been updated.", entity.getClass().getName(), entity.getVer()));
            }
        } else {
            sessionTemplate.update(mapper.getMapperClass().concat(".").concat(mapper.getInsertMethodName()), entity);
        }

        return entity.getPrimaryKeyValue();
    }

    @Override
    public <T extends BaseSO> T loadOne(Class<T> entityClass, Serializable entityId) {
        MapperInfo mapper = getMapperAnnotation(entityClass);
        return sessionTemplate.selectOne(mapper.getMapperClass().concat(".").concat(mapper.getSelectOneMethodName()), entityId);
    }

    @Override
    public <T extends BaseSO> List<T> loadList(Class<T> entityClass, Serializable entityId) {
        MapperInfo mapper = getMapperAnnotation(entityClass);
        return sessionTemplate.selectList(mapper.getMapperClass().concat(".").concat(mapper.getSelectListMethodName()), entityId);
    }


    private static MapperInfo getMapperAnnotation(Class entity) {
        if (entity.isAnnotationPresent(StorageMapper.class)) {
            StorageMapper mapper = (StorageMapper)entity.getAnnotation(StorageMapper.class);
            MapperInfo info = new MapperInfo();
            info.setMapperClass(mapper.value().getName());
            info.setAliasName(mapper.aliasName());
            info.setInsertMethodName(mapper.insertMethodName());
            info.setSelectListMethodName(mapper.selectListMethodName());
            info.setSelectOneMethodName(mapper.selectOneMethodName());
            info.setUpdateMethodName(mapper.updateMethodName());
            return info;
        } else {
            throw new StorageException("Storage object [${entity.class}] must be annotated with com.kry.infra.repository.mybatis.StorageMapper");
        }
    }

    @Setter
    @Getter
    private static class MapperInfo {
        private String mapperClass;
        private String insertMethodName;
        private String updateMethodName;
        private String aliasName;
        private String selectOneMethodName;
        private String selectListMethodName;
    }
}
