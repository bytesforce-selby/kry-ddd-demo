package com.kry.adapter.web;

import lombok.Data;

@Data
public class Response {

    private boolean success = false;

    private String errorCode;
    private String errorMessage;

    private Object data;

    public static Response newWithFailed(String errorCode, String errorMessage) {
        Response result = new Response();
        result.success = false;
        result.errorCode = errorCode;
        result.errorMessage = errorMessage;
        return result;
    }

    public static Response newWithFailedByDefault() {
        Response result = new Response();
        result.success = false;
        result.errorMessage = "Opps! please contact your system administrator for help.";
        return result;
    }

    public static Response newWithSuccessWithData(Object data) {
        Response result = new Response();
        result.success = true;
        result.data = data;
        return result;
    }

}
