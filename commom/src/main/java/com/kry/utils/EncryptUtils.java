package com.kry.utils;

import com.kry.exception.EncryptException;
import org.springframework.util.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;
import java.util.Base64;

public final class EncryptUtils {

    private static final String DEFAULT_PASSWORD = "kry#$@%332";

    private static final String ALGORITHM_AES = "AES";
    private static final String ALGORITHM_SHA = "SHA1PRNG";
    private static final String DEFAULT_CIPHER_ALGORITHM_AES = "AES/ECB/PKCS5Padding";
    private static final String DEFAULT_CHARSET = "UTF-8";

    private EncryptUtils() {
    }

    public static String encryptWithAES256(String content) {
        if (StringUtils.isEmpty(content)) {
            return null;
        }
        try {
            Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM_AES);
            cipher.init(Cipher.ENCRYPT_MODE, getSecretKey());
            byte[] result = cipher.doFinal(content.getBytes(DEFAULT_CHARSET));
            return Base64.getEncoder().encodeToString(result);
        } catch (Exception e) {
            throw new EncryptException(e);
        }
    }

    public static String decryptWithAES256(String content) {
        if (StringUtils.isEmpty(content)) {
            return null;
        }
        try {
            Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM_AES);
            cipher.init(Cipher.DECRYPT_MODE, getSecretKey());
            byte[] result = cipher.doFinal(Base64.getDecoder().decode(content));
            return new String(result, DEFAULT_CHARSET);
        } catch (Exception e) {
            throw new EncryptException(e);
        }
    }

    private static SecretKeySpec getSecretKey() {
        try {
            KeyGenerator kg = KeyGenerator.getInstance(ALGORITHM_AES);
            SecureRandom secureRandom = SecureRandom.getInstance(ALGORITHM_SHA);
            secureRandom.setSeed(DEFAULT_PASSWORD.getBytes(DEFAULT_CHARSET));
            kg.init(256, secureRandom);
            return new SecretKeySpec(kg.generateKey().getEncoded(), ALGORITHM_AES);
        } catch (Exception e){
            throw new EncryptException(e);
        }
    }
}
