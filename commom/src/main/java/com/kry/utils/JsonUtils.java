package com.kry.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kry.exception.ConvertException;
import org.springframework.util.StringUtils;

import java.util.List;

public class JsonUtils {

    private static ObjectMapper mapper;

    private JsonUtils() {
    }

    static {
        mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static String toJson(Object object) {
        if (object == null) {
            return null;
        }
        try {
            return mapper.writeValueAsString(object);
        } catch (Exception e) {
            throw new ConvertException("Fail to convert to json.", e);
        }

    }

    public static <T> T fromJson(Class<T> target, String json) {
        if (StringUtils.isEmpty(json)) {
            return null;
        }
        try {
            return mapper.readValue(json, target);
        } catch (Exception e) {
            throw new ConvertException("Fail to convert from json.", e);
        }

    }

    public static <T> List<T> fromJsonAsList(Class<T> target, String json) {
        if (StringUtils.isEmpty(json)) {
            return null;
        }
        try {
            JavaType type = mapper.getTypeFactory().
                    constructCollectionType(List.class, target);
            return (List<T>) mapper.readValue(json, type);
        } catch (Exception e) {
            throw new ConvertException("Fail to convert from json.", e);
        }
    }
}
