package com.kry.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix = "kry")
public class KryProperties {

    private static final String DEFAULT_TOPIC_PREFIX = "KRY";
    /**
     * Set prefix of topic when using MoM. By default this value is KRY.
     */
    private String topicPrefix = DEFAULT_TOPIC_PREFIX;

}
