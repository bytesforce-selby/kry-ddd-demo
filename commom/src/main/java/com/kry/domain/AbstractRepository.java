package com.kry.domain;

import com.kry.config.KryProperties;
import com.kry.exception.AggregateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;

public abstract class AbstractRepository<T extends Aggregate> implements Repository<T>{

    @Autowired
    private KryProperties properties;

    @Override
    public void save(T aggregate) {
        notNullOrEmpty(aggregate.getEntityId());
        onlyOneEventAllowed(aggregate);
        saveAggregate(aggregate);
        publishEvent(aggregate);
    }

    protected abstract void saveAggregate(T aggregate);

    protected abstract void publishEvent(T aggregate);

    protected DomainEvent getEvent(T aggregate) {
        DomainEvent event = null;
        if (!CollectionUtils.isEmpty(aggregate.allEvents())) {
            event = aggregate.allEvents().get(0);
        }
        return event;
    }

    protected String getDefaultTopicName(DomainEvent event) {
        String[] names = event.getClass().getName().split("\\.");
        return properties.getTopicPrefix().concat(".").concat(names[names.length-1]);
    }

    private static void notNullOrEmpty(Serializable aggregatorId) {
        if (StringUtils.isEmpty(aggregatorId)) {
            throw new AggregateException("Aggregate id cannot be empty.");
        }
    }

    private void onlyOneEventAllowed(T aggregate) {
        if (aggregate.allEvents() != null && aggregate.allEvents().size() > 1) {
            throw new AggregateException("Only one event is allowed for one operation.");
        }
    }


}
