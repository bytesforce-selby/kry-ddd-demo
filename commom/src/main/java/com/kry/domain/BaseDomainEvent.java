package com.kry.domain;

import org.joda.time.DateTime;

import java.util.Date;
import java.util.UUID;

public class BaseDomainEvent implements DomainEvent {

    public String getEventId() {
        return UUID.randomUUID().toString();
    }

    public Date getOccurredDate() {
        return DateTime.now().toDate();
    }

}
