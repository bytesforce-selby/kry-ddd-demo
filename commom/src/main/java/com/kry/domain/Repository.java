package com.kry.domain;

import java.io.Serializable;

/**
 * Responsibility: Store and loadOne aggregate
 * <p>
 * 1. Convert between aggregate and data storage object
 * 2. Persist aggregate
 * 3. Cache should be here as well if necessary
 *
 * All implementation of repository should inherit AbstractRepository class.
 */
public interface Repository<T extends Aggregate> {

    /**
     * Persist aggregate to storage.
     * @param aggregate
     */
    void save(T aggregate);


    /**
     * Load aggregate from storage.
     * If there is no aggregate found, runtime exception will be thrown.
     * @param aggregateId
     * @return
     */
    T load(Serializable aggregateId);

}
