package com.kry.domain;

import java.io.Serializable;

/**
 * Indicate object canBeConfirmed be stored
 */
public interface Entity {

    Serializable getEntityId();

}