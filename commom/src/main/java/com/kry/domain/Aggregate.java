package com.kry.domain;

import com.kry.SpringUtils;
import com.kry.exception.AggregateException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class Aggregate implements Entity {

    private List<DomainEvent> events = new ArrayList<>();

    public final void applyEvent(DomainEvent event) {
        if (event == null) {
            throw new AggregateException("Event must be provided.");
        }
        if (events.size() > 0) {
            throw new AggregateException("Only one event is allowed for one operation.");
        }
        events.add(event);
    }

    @Override
    public final Serializable getEntityId() {
        return getAggregateId();
    }

    public abstract Serializable getAggregateId();

    public final List<DomainEvent> allEvents() {
        return this.events;
    }

    public final void clearEvents() {
        events.clear();
    }


    protected static  <T> T getBean(Class<T> clazz) {
        return SpringUtils.getBean(clazz);
    }

    protected static <T> T getBean(Class<T> clazz, String beanName) {
        return SpringUtils.getBean(clazz, beanName);
    }

}
