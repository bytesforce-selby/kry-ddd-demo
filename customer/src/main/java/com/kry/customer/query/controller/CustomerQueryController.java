//package com.kry.customer.query.controller;
//
//import com.kry.customer.query.dataobjects.ReceiverDO;
//import com.kry.customer.query.dao.CustomerDAO;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.MediaType;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@Api(description = "Query customer's information", tags = "200 - Customer Query APIs")
//@RestController
//@RequestMapping("/queries/customer")
//public class CustomerQueryController {
//
//    @Autowired
//    private CustomerDAO customerDAO;
//
//
//    @ResponseBody
//    @ApiOperation(value = "Get all receivers of customer.", notes = "Get all receivers of customer.")
//    @GetMapping(value = "/{customerId}/receivers", produces =  MediaType.APPLICATION_JSON_UTF8_VALUE)
//    public List<ReceiverDO> findAllReceivers(@PathVariable String customerId) {
//        return customerDAO.findAllReceivers(customerId);
//    }
//
//
//}
