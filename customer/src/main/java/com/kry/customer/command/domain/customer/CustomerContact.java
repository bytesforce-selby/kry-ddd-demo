package com.kry.customer.command.domain.customer;

import com.kry.domain.ValueObject;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class CustomerContact implements ValueObject {

    private String mobile;
    private String email;

    public CustomerContact changeMobile(String newMobile) {
        return CustomerContact.builder().email(email).mobile(newMobile).build();
    }

    public static CustomerContact newWithMobile(String newMobile) {
        return CustomerContact.builder().mobile(newMobile).build();
    }

}
