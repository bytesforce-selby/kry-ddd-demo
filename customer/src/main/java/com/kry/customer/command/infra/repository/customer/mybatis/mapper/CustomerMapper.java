package com.kry.customer.command.infra.repository.customer.mybatis.mapper;

import com.kry.customer.command.infra.repository.customer.storageobjects.CustomerSO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.Map;

@Mapper
public interface CustomerMapper {

    @Insert({
            "insert into customer (customer_id, birthday, ",
            "gender, real_name, ",
            "mobile, email, create_date, ",
            "create_user, update_date, ",
            "update_user, ver)",
            "values (#{customerId,jdbcType=VARCHAR}, #{birthday,jdbcType=TIMESTAMP}, ",
            "#{gender,jdbcType=VARCHAR}, #{realName,jdbcType=VARCHAR}, ",
            "#{mobile,jdbcType=VARCHAR}, #{email,jdbcType=VARCHAR}, #{createDate,jdbcType=TIMESTAMP}, ",
            "#{createUser,jdbcType=VARCHAR}, #{updateDate,jdbcType=TIMESTAMP}, ",
            "#{updateUser,jdbcType=VARCHAR}, #{ver,jdbcType=TINYINT})"
    })
    int insert(CustomerSO record);


    @Select({
            "select",
            "customer_id, birthday, gender, real_name, mobile, email, create_date, create_user, ",
            "update_date, update_user, ver",
            "from customer",
            "where customer_id = #{customerId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column="customer_id", property="customerId", jdbcType=JdbcType.VARCHAR, id=true),
            @Result(column="birthday", property="birthday", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="gender", property="gender", jdbcType=JdbcType.VARCHAR),
            @Result(column="real_name", property="realName", jdbcType=JdbcType.VARCHAR),
            @Result(column="mobile", property="mobile", jdbcType=JdbcType.VARCHAR),
            @Result(column="email", property="email", jdbcType=JdbcType.VARCHAR),
            @Result(column="create_date", property="createDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="create_user", property="createUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="update_date", property="updateDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="update_user", property="updateUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="ver", property="ver", jdbcType=JdbcType.TINYINT)
    })
    CustomerSO selectByPrimaryKey(String accountId);

    @Update({
            "update customer",
            "set birthday = #{entity.birthday,jdbcType=TIMESTAMP},",
            "gender = #{entity.gender,jdbcType=VARCHAR},",
            "real_name = #{entity.realName,jdbcType=VARCHAR},",
            "mobile = #{entity.mobile,jdbcType=VARCHAR},",
            "email = #{entity.email,jdbcType=VARCHAR},",
            "create_date = #{entity.createDate,jdbcType=TIMESTAMP},",
            "create_user = #{entity.createUser,jdbcType=VARCHAR},",
            "update_date = #{entity.updateDate,jdbcType=TIMESTAMP},",
            "update_user = #{entity.updateUser,jdbcType=VARCHAR},",
            "ver = #{entity.ver,jdbcType=TINYINT}",
            "where customer_id = #{entity.customerId,jdbcType=VARCHAR} and ver = #{ver,jdbcType=TINYINT}"
    })
    int updateByPrimaryKey(Map<String, Object> params);

}
