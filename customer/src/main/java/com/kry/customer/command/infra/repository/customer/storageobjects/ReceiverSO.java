package com.kry.customer.command.infra.repository.customer.storageobjects;

import com.kry.customer.command.infra.repository.customer.mybatis.mapper.ReceiverMapper;
import com.kry.infra.repository.BaseSO;
import com.kry.infra.repository.mybatis.annotation.StorageMapper;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@StorageMapper(ReceiverMapper.class)
@Accessors(chain = true)
@Data
public class ReceiverSO extends BaseSO {

    private String receiverId;
    private String receiverName;
    private String receiverMobile;

    private String province;
    private String city;
    private String district;
    private String street;
    private String buildingAndNumber;

    private String customerId;

    private String isDefault;

    @Override
    public Serializable getPrimaryKeyValue() {
        return receiverId;
    }
}
