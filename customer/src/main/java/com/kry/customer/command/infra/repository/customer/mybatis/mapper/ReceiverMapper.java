package com.kry.customer.command.infra.repository.customer.mybatis.mapper;

import com.kry.customer.command.infra.repository.customer.storageobjects.ReceiverSO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;
import java.util.Map;

@Mapper
public interface ReceiverMapper {

    @Insert({
            "insert into receiver (receiver_id, name, is_default, ",
            "mobile, customer_id, ",
            "province, city, ",
            "district, street, ",
            "building_and_number, create_date, ",
            "create_user, update_date, ",
            "update_user, ver)",
            "values (#{receiverId,jdbcType=VARCHAR}, #{receiverName, jdbcType=VARCHAR}, #{isDefault, jdbcType=VARCHAR},",
            "#{receiverMobile,jdbcType=VARCHAR}, #{customerId,jdbcType=VARCHAR}, ",
            "#{province,jdbcType=VARCHAR}, #{city,jdbcType=VARCHAR}, ",
            "#{district,jdbcType=VARCHAR}, #{street,jdbcType=VARCHAR}, ",
            "#{buildingAndNumber,jdbcType=VARCHAR}, #{createDate,jdbcType=TIMESTAMP}, ",
            "#{createUser,jdbcType=VARCHAR}, #{updateDate,jdbcType=TIMESTAMP}, ",
            "#{updateUser,jdbcType=VARCHAR}, #{ver,jdbcType=TINYINT})"
    })
    int insert(ReceiverSO record);


    @Select({
            "select",
            "receiver_id, name, mobile, customer_id, province, city, district, street, building_and_number, ",
            "create_date, create_user, update_date, update_user, ver, is_default ",
            "from receiver",
            "where receiver_id = #{receiverId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column="receiver_id", property="receiverId", jdbcType=JdbcType.VARCHAR, id=true),
            @Result(column="name", property="receiverName", jdbcType=JdbcType.VARCHAR),
            @Result(column="mobile", property="receiverMobile", jdbcType=JdbcType.VARCHAR),
            @Result(column="customer_id", property="customerId", jdbcType=JdbcType.VARCHAR),
            @Result(column="province", property="province", jdbcType=JdbcType.VARCHAR),
            @Result(column="city", property="city", jdbcType=JdbcType.VARCHAR),
            @Result(column="district", property="district", jdbcType=JdbcType.VARCHAR),
            @Result(column="street", property="street", jdbcType=JdbcType.VARCHAR),
            @Result(column="building_and_number", property="buildingAndNumber", jdbcType=JdbcType.VARCHAR),
            @Result(column="is_default", property="isDefault", jdbcType=JdbcType.VARCHAR),
            @Result(column="create_date", property="createDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="create_user", property="createUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="update_date", property="updateDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="update_user", property="updateUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="ver", property="ver", jdbcType=JdbcType.TINYINT)
    })
    ReceiverSO selectByPrimaryKey(String receiverId);

    @Select({
            "select",
            "receiver_id, name, mobile, customer_id, province, city, district, street, building_and_number, ",
            "create_date, create_user, update_date, update_user, ver, is_default ",
            "from receiver",
            "where customer_id = #{customerId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column="receiver_id", property="receiverId", jdbcType=JdbcType.VARCHAR, id=true),
            @Result(column="name", property="receiverName", jdbcType=JdbcType.VARCHAR),
            @Result(column="mobile", property="receiverMobile", jdbcType=JdbcType.VARCHAR),
            @Result(column="customer_id", property="customerId", jdbcType=JdbcType.VARCHAR),
            @Result(column="province", property="province", jdbcType=JdbcType.VARCHAR),
            @Result(column="city", property="city", jdbcType=JdbcType.VARCHAR),
            @Result(column="district", property="district", jdbcType=JdbcType.VARCHAR),
            @Result(column="street", property="street", jdbcType=JdbcType.VARCHAR),
            @Result(column="building_and_number", property="buildingAndNumber", jdbcType=JdbcType.VARCHAR),
            @Result(column="is_default", property="isDefault", jdbcType=JdbcType.VARCHAR),
            @Result(column="create_date", property="createDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="create_user", property="createUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="update_date", property="updateDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="update_user", property="updateUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="ver", property="ver", jdbcType=JdbcType.TINYINT)
    })
    List<ReceiverSO> selectByAggregateId(String customerId);

    @Update({
            "update receiver",
            "set name = #{entity.receiverName, jdbcType=VARCHAR},",
            "mobile = #{entity.receiverMobile,jdbcType=VARCHAR},",
            "customer_id = #{entity.customerId,jdbcType=VARCHAR},",
            "province = #{entity.province,jdbcType=VARCHAR},",
            "city = #{entity.city,jdbcType=VARCHAR},",
            "district = #{entity.district,jdbcType=VARCHAR},",
            "street = #{entity.street,jdbcType=VARCHAR},",
            "building_and_number = #{entity.buildingAndNumber,jdbcType=VARCHAR},",
            "is_default = #{entity.isDefault,jdbcType=VARCHAR},",
            "create_date = #{entity.createDate,jdbcType=TIMESTAMP},",
            "create_user = #{entity.createUser,jdbcType=VARCHAR},",
            "update_date = #{entity.updateDate,jdbcType=TIMESTAMP},",
            "update_user = #{entity.updateUser,jdbcType=VARCHAR},",
            "ver = #{entity.ver,jdbcType=TINYINT}",
            "where receiver_id = #{entity.receiverId,jdbcType=VARCHAR} and ver = #{ver,jdbcType=TINYINT}"
    })
    int updateByPrimaryKey(Map<String, Object> params);

}
