package com.kry.customer.command.application.valueobjects;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Accessors(chain = true)
@Data
public class ReceiverBaseVO {

    @NotNull(message = "receiverName must be provided.")
    private String receiverName;
    @NotNull(message = "receiverMobile must be provided.")
    private String receiverMobile;

    @NotNull(message = "province must be provided.")
    private String province;
    @NotNull(message = "city must ve provided.")
    private String city;
    @NotNull(message = "district must be provided.")
    private String district;
    @NotNull(message = "street must be provided.")
    private String street;
    @NotNull(message = "buildingAndNumber must be provided.")
    private String buildingAndNumber;

}
