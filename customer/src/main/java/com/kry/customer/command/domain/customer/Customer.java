package com.kry.customer.command.domain.customer;

import com.google.common.collect.Lists;
import com.kry.customer.command.application.valueobjects.CustomerBasicInfoVO;
import com.kry.customer.command.application.valueobjects.ReceiverChangeVO;
import com.kry.customer.command.application.valueobjects.ReceiverCreationVO;
import com.kry.customer.command.exception.ReceiverNotFoundException;
import com.kry.customer.command.exception.ReceiverOutOfRangeException;
import com.kry.domain.Aggregate;
import lombok.Builder;
import lombok.Getter;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Builder
@Getter
public class Customer extends Aggregate {

    private String id;
    private String realName;
    private Date birthday;
    private Gender gender;
    private CustomerContact contact;

    private List<Receiver> receivers;


    public void createCustomerWithAccount(String accountId) {
        this.id = accountId;
    }

    public void changeBasicInfo(CustomerBasicInfoVO basicInfo) {
        this.realName = basicInfo.getRealName();
        this.birthday = basicInfo.getBirthday();
        this.gender = Gender.from(basicInfo.getGender());
    }

    public void changeMobile(String mobile) {
        if (contact == null) {
            contact = CustomerContact.newWithMobile(mobile);
        } else  {
            contact = contact.changeMobile(mobile);
        }
    }

    public void addReceiver(ReceiverCreationVO receiverVO) {
        if (receivers == null) {
            receivers = Lists.newArrayList();
        }
        if (receivers.size() == 5) {
            throw new ReceiverOutOfRangeException(5);
        }

        Receiver receiver = Receiver.builder()
                .id(UUID.randomUUID().toString())
                .name(receiverVO.getReceiverName())
                .contact(ReceiverContact.builder()
                        .mobile(receiverVO.getReceiverMobile()).build())
                .address(Address.builder().province(receiverVO.getProvince())
                        .city(receiverVO.getCity())
                        .district(receiverVO.getDistrict())
                        .street(receiverVO.getStreet())
                        .buildingAndNumber(receiverVO.getBuildingAndNumber()).build())
                .build();
        receivers.add(receiver);
    }

    public void changeReceiver(String receiverId, ReceiverChangeVO receiverVO) {
        if (!CollectionUtils.isEmpty(receivers)) {
            Optional<Receiver> receiver = receivers.stream().filter(it -> it.getId().equals(receiverId)).findAny();
            if (!receiver.isPresent()) {
                throw new ReceiverNotFoundException(receiverId);
            }
            receiver.get().change(receiverVO);
        }
    }

    public Receiver defaultReceiver() {
        if (!CollectionUtils.isEmpty(receivers)) {
            Optional<Receiver> receiver = receivers.stream().filter(it -> it.isDefaultOne()).findAny();
            if (receiver.isPresent()) {
                return receiver.get();
            }
        }
        return null;
    }

    public void setDefaultReceiver(String receiverId) {
        if (!CollectionUtils.isEmpty(receivers)) {
            receivers.stream().forEach(receiver -> {
                if (receiver.isDefaultOne()) {
                    receiver.cancelDefault();
                }
            });

            Optional<Receiver> receiver = receivers.stream().filter(it -> it.getId().equals(receiverId)).findAny();
            if (!receiver.isPresent()) {
                throw new ReceiverNotFoundException(receiverId);
            }
            receiver.get().setAsDefault();
        }
    }


    @Override
    public Serializable getAggregateId() {
        return id;
    }

}
