package com.kry.customer.command.infra.repository.customer;

import com.google.common.collect.Lists;
import com.kry.customer.command.domain.customer.CustomerContact;
import com.kry.customer.command.domain.customer.Customer;
import com.kry.customer.command.domain.customer.CustomerRepository;
import com.kry.customer.command.domain.customer.Gender;
import com.kry.customer.command.domain.customer.Address;
import com.kry.customer.command.domain.customer.Receiver;
import com.kry.customer.command.domain.customer.ReceiverContact;
import com.kry.customer.command.infra.repository.customer.storageobjects.CustomerSO;
import com.kry.customer.command.infra.repository.customer.storageobjects.ReceiverSO;
import com.kry.domain.AbstractRepository;
import com.kry.domain.DomainEvent;
import com.kry.exception.AggregateNotFoundException;
import com.kry.infra.repository.DaoProvider;
import com.kry.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.List;

@Repository
public class CustomerRepositoryImpl extends AbstractRepository<Customer> implements CustomerRepository {

    @Autowired
    private DaoProvider daoProvider;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Override
    protected void saveAggregate(Customer aggregate) {
        storeCustomer(aggregate);
        storeReceiver(aggregate);
    }


    private void storeCustomer(Customer aggregate) {
        CustomerSO customerSO = new CustomerSO();
        customerSO.setBirthday(aggregate.getBirthday()).setCustomerId(aggregate.getId())
                .setRealName(aggregate.getRealName());
        if (aggregate.getGender() != null) {
            customerSO.setGender(aggregate.getGender().toString());
        }
        if (aggregate.getContact() != null) {
            customerSO.setEmail(aggregate.getContact().getEmail()).setMobile(aggregate.getContact().getMobile());
        }
        daoProvider.save(customerSO);
    }

    private void storeReceiver(Customer aggregate) {
        if (!CollectionUtils.isEmpty(aggregate.getReceivers())) {
            aggregate.getReceivers().stream().forEach( receiver -> {

                ReceiverSO receiverSO = new ReceiverSO();
                receiverSO.setBuildingAndNumber(receiver.getAddress().getBuildingAndNumber())
                        .setCity(receiver.getAddress().getCity()).setDistrict(receiver.getAddress().getDistrict())
                        .setProvince(receiver.getAddress().getProvince()).setStreet(receiver.getAddress().getStreet());

                receiverSO.setCustomerId(aggregate.getId()).setReceiverId(receiver.getId()).setReceiverName(receiver.getName());

                receiverSO.setReceiverMobile(receiver.getContact().getMobile());

                receiverSO.setIsDefault(receiver.isDefaultOne() ? "Y" : "N");

                daoProvider.save(receiverSO);
            });
        }

    }

    @Override
    protected void publishEvent(Customer aggregate) {
        DomainEvent event = getEvent(aggregate);
        if (event != null) {
            kafkaTemplate.send(getDefaultTopicName(event), aggregate.getEntityId().toString(), JsonUtils.toJson(event));
        }
    }

    @Override
    public Customer load(Serializable aggregateId) {
        CustomerSO customerSO = daoProvider.loadOne(CustomerSO.class, aggregateId);
        if (customerSO == null) {
            throw new AggregateNotFoundException();
        }

        List<ReceiverSO> receiverSOList = daoProvider.loadList(ReceiverSO.class, aggregateId);

        List<Receiver> receivers = Lists.newArrayList();
        if (!CollectionUtils.isEmpty(receiverSOList)) {
            receiverSOList.stream().forEach(receiverSO -> {
                Receiver receiver = Receiver.builder().id(receiverSO.getReceiverId()).name(receiverSO.getReceiverName())
                        .contact(ReceiverContact.builder().mobile(receiverSO.getReceiverMobile()).build())
                        .address(Address.builder().buildingAndNumber(receiverSO.getBuildingAndNumber())
                                .street(receiverSO.getStreet())
                                .district(receiverSO.getDistrict())
                                .city(receiverSO.getCity())
                                .province(receiverSO.getProvince()).build())
                        .defaultOne("Y".equals(receiverSO.getIsDefault()))
                        .build();
                receivers.add(receiver);
            });
        }

        Customer customer = Customer.builder().birthday(customerSO.getBirthday())
                .gender(customerSO.getGender() != null ? Gender.from(customerSO.getGender()) : null)
                .id(customerSO.getCustomerId()).realName(customerSO.getRealName())
                .contact(CustomerContact.builder().email(customerSO.getEmail())
                        .mobile(customerSO.getMobile()).build())
                .receivers(receivers)
                .build();

        return customer;
    }

}
