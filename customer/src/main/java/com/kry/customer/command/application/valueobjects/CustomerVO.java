package com.kry.customer.command.application.valueobjects;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Accessors(chain = true)
@Data
public class CustomerVO {

    private String customerId;
    private String realName;
    private Date birthday;
    private String mobile;
    private String email;
    private String gender;

}
