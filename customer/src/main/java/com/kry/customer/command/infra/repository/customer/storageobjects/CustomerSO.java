package com.kry.customer.command.infra.repository.customer.storageobjects;

import com.kry.customer.command.infra.repository.customer.mybatis.mapper.CustomerMapper;
import com.kry.infra.repository.BaseSO;
import com.kry.infra.repository.mybatis.annotation.StorageMapper;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@StorageMapper(CustomerMapper.class)
@Accessors(chain = true)
@Data
public class CustomerSO extends BaseSO {

    private String customerId;
    private String realName;
    private String mobile;
    private String email;
    private Date birthday;
    private String gender;

    @Override
    public Serializable getPrimaryKeyValue() {
        return customerId;
    }
}
