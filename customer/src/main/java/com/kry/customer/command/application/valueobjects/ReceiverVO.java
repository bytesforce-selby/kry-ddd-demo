package com.kry.customer.command.application.valueobjects;

import lombok.Data;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Data
public class ReceiverVO extends ReceiverBaseVO{

    private String receiverId;

}
