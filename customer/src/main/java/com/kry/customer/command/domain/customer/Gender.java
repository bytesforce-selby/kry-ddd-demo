package com.kry.customer.command.domain.customer;

import com.kry.exception.UnrecognizedException;
import org.springframework.util.StringUtils;

public enum Gender {

    MALE("M"), FEMALE("F");

    private String gender;

    private static final String M = "M";
    private static final String F = "F";

    Gender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return gender;
    }

    public static Gender from(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        if (M.equals(value)) {
            return MALE;
        } else if (F.equals(value)) {
            return FEMALE;
        } else {
            throw new UnrecognizedException();
        }
    }

}
