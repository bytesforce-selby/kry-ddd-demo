package com.kry.customer.command.exception;

public class ReceiverNotFoundException extends RuntimeException {

    public ReceiverNotFoundException(String receiverId) {
        super(String.format("Receiver not found with id [%s]", receiverId));
    }
}
