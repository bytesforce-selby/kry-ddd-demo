package com.kry.customer.command.domain.customer;

import com.kry.customer.command.application.valueobjects.ReceiverChangeVO;
import com.kry.domain.Entity;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

@Builder
@Getter
public class Receiver implements Entity {

    private String id;
    private String name;
    private ReceiverContact contact;
    private Address address;
    private boolean defaultOne;


    public void change(ReceiverChangeVO changeVO) {
        this.name = changeVO.getReceiverName();
        this.contact = ReceiverContact.builder().mobile(changeVO.getReceiverMobile()).build();
        this.address = Address.builder().province(changeVO.getProvince()).city(changeVO.getCity())
                .district(changeVO.getDistrict()).street(changeVO.getStreet())
                .buildingAndNumber(changeVO.getBuildingAndNumber()).build();
    }

    public void setAsDefault() {
        defaultOne = true;
    }

    public void cancelDefault() {
        defaultOne = false;
    }

    @Override
    public Serializable getEntityId() {
        return id;
    }
}
