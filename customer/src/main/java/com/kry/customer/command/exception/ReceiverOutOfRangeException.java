package com.kry.customer.command.exception;

import com.kry.exception.BusinessException;

public class ReceiverOutOfRangeException extends BusinessException {

    public ReceiverOutOfRangeException(int max) {
        super(ErrorCode.RECEIVER_OUT_OF_RANGE, String.format("Allowed the max of receiver is %s", max));
    }
}
