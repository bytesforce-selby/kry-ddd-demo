package com.kry.trade.command.domain.order;

import com.kry.exception.UnrecognizedException;
import org.springframework.util.StringUtils;

public enum  PaymentMode {

    ONLINE("OL"), OFFLINE("OFL");

    private String mode;

    PaymentMode(String mode) {
        this.mode = mode;
    }

    public String value() {
        return mode;
    }

    public static PaymentMode from(String value) {

        if (StringUtils.isEmpty(value)) {
            return null;
        }

        if (value.equals("OL")) {
            return ONLINE;
        } else if (value.equals("OFL")) {
            return OFFLINE;
        } else {
            throw new UnrecognizedException();
        }
    }
}
