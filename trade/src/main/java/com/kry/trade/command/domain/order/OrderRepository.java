package com.kry.trade.command.domain.order;

import com.kry.domain.Repository;
import com.kry.trade.command.domain.order.Order;

public interface OrderRepository extends Repository<Order>{

}
