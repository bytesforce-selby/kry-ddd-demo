package com.kry.trade.command.domain.order;

import com.kry.domain.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.math.BigDecimal;

@Builder
@Getter
public class OrderItem implements Entity {

    private String itemId;
    private String productId;
    private String productName;
    private Integer quantity;
    private BigDecimal price;
    private BigDecimal totalAmount;

    public void changeQuantity(int quantity) {
        this.quantity = quantity;
        calculateTotalAmount();
    }

    public void calculateTotalAmount() {
        totalAmount = price.multiply(BigDecimal.valueOf(quantity));
    }

    @Override
    public Serializable getEntityId() {
        return this.itemId;
    }


}