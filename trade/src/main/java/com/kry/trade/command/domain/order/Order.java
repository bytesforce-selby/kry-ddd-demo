package com.kry.trade.command.domain.order;

import com.google.common.collect.Lists;
import com.kry.domain.Aggregate;
import com.kry.trade.command.application.valueobjects.OrderItemCreationVO;
import com.kry.trade.command.application.valueobjects.ReceiverVO;
import com.kry.trade.command.exception.IllegalOrderOperationException;
import com.kry.trade.command.exception.InvalidQuantityException;
import com.kry.trade.command.exception.OrderItemNotFoundException;
import com.kry.utils.KryAssert;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Builder
@Getter
public class Order extends Aggregate {

    private String orderId;

    private BigDecimal originAmount;
    private BigDecimal discountAmount;
    private BigDecimal finalAmount;

    private List<OrderItem> items;

    private Receiver receiver;
    private PaymentMode paymentMode;

    private Customer customer;

    private OrderStatus status;


    public static Order buildWithItems(List<OrderItemCreationVO> itemList) {
        List<OrderItem> items = Lists.newArrayList();
        for (OrderItemCreationVO itemVO : itemList) {
            OrderItem item = OrderItem.builder().itemId(UUID.randomUUID().toString())
                    .price(itemVO.getPrice())
                    .productId(itemVO.getProductId())
                    .productName(itemVO.getProductName())
                    .quantity(itemVO.getQuantity())
                    .build();
            item.calculateTotalAmount();
            items.add(item);
        }
        return Order.builder().items(items).build();
    }

    public void create() {
        KryAssert.notEmpty(items, "Items of order must be built when building order.");

        orderId = UUID.randomUUID().toString();
        status = OrderStatus.CREATED;

        // Get default receiver
//        receiver = getBean(OrderDomainService.class).getDefaultReceiver(
//                AppContext.currentUser().getUserId().toString()
//        );
        receiver = getBean(OrderDomainService.class).getDefaultReceiver(
                "250a2cb6-cfc4-486a-a7dc-020a9e934fab"
        );
        paymentMode = PaymentMode.ONLINE;
//        customer = new Customer(AppContext.currentUser().getUserId().toString(),
//                AppContext.currentUser().getUserName());

        customer = new Customer("250a2cb6-cfc4-486a-a7dc-020a9e934fab",
                "Selby");

        // TODO Book inventory
        getBean(OrderDomainService.class).bookInventory(orderId, items);

        recalculateAmount();

        applyEvent(OrderCreatedEvent.builder().orderId(orderId).build());
    }

    public void confirm() {
        if (status != OrderStatus.CREATED) {
            throw new IllegalOrderOperationException(
                    "Confirmation cannot be done because order status is not CREATED."
            );
        }

        if (paymentMode == PaymentMode.ONLINE) {
            status = OrderStatus.WAITING_FOR_PAY;
        } else {
            status = OrderStatus.WAITING_FOR_SHIPPING;
        }

        applyEvent(new OrderConfirmedEvent(orderId, status.value()));
    }

    public void cancel() {
        if (status != OrderStatus.CREATED) {
            throw new IllegalOrderOperationException(
                    "Cancellation can be done because order status is CREATED."
            );
        }
        status = OrderStatus.CANCELED;
        applyEvent(new OrderCancelledEvent(orderId));
    }

    public void changeReceiver(ReceiverVO receiverVO) {
        receiver = Receiver.create(receiverVO);
    }

    public void changePaymentMode(String mode) {
        checkIfCreatedStatus();
        paymentMode = PaymentMode.from(mode);
    }

    public void changeItemQuantity(String itemId, int quantity) {
        checkIfCreatedStatus();
        if (quantity <= 0) {
            throw new InvalidQuantityException(quantity);
        }
        Optional<OrderItem> item = items.stream().filter(orderItem -> orderItem.getItemId().equals(itemId)).findAny();
        if (!item.isPresent()) {
            throw new OrderItemNotFoundException(itemId);
        }
        item.get().changeQuantity(quantity);
        recalculateAmount();
    }

    private void recalculateAmount() {
        BigDecimal newOriginAmount = BigDecimal.ZERO;
        for (OrderItem item : items) {
            newOriginAmount = newOriginAmount.add(item.getTotalAmount());
        }
        originAmount = newOriginAmount;
        if (discountAmount == null) {
            discountAmount = BigDecimal.ZERO;
        }
        finalAmount = originAmount.subtract(discountAmount);
    }

    private void checkIfCreatedStatus() {
        if (status != OrderStatus.CREATED) {
            throw new IllegalOrderOperationException(
                    String.format("Status is not %s, cannot change.", OrderStatus.CREATED)
            );
        }
    }

    @Override
    public Serializable getAggregateId() {
        return orderId;
    }
}
