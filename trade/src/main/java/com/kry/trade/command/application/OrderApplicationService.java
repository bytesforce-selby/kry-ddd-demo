package com.kry.trade.command.application;

import com.kry.trade.command.application.valueobjects.OrderCreationVO;
import com.kry.trade.command.application.valueobjects.OrderItemVO;
import com.kry.trade.command.application.valueobjects.OrderVO;
import com.kry.trade.command.application.valueobjects.ReceiverVO;
import com.kry.trade.command.domain.order.Order;
import com.kry.trade.command.domain.order.OrderItem;
import com.kry.trade.command.domain.order.OrderRepository;
import com.kry.utils.KryAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class OrderApplicationService {

    @Autowired
    private OrderRepository orderRepository;


    public void createOrder(OrderCreationVO orderVO) {
        Order order = Order.buildWithItems(orderVO.getItems());
        order.create();
        orderRepository.save(order);
    }

    public void confirm(String orderId) {
        KryAssert.notEmpty(orderId, "orderId must be provided.");

        Order order = orderRepository.load(orderId);
        order.confirm();

        orderRepository.save(order);
    }

    public void cancel(String orderId) {
        KryAssert.notEmpty(orderId, "orderId must be provided.");

        Order order = orderRepository.load(orderId);
        order.cancel();

        orderRepository.save(order);
    }

    public void changeQuantityOfItem(String orderId, String itemId, int quantity) {
        KryAssert.notEmpty(orderId, "orderId must be provided.");
        KryAssert.notEmpty(itemId, "itemId must be provided.");
        KryAssert.isTrue(quantity > 0, "quantity must be greater than zero.");

        Order order = orderRepository.load(orderId);
        order.changeItemQuantity(itemId, quantity);

        orderRepository.save(order);
    }


    public void changeReceiver(String orderId, ReceiverVO receiverVO) {
        KryAssert.notEmpty(orderId, "orderId must be provided.");
        KryAssert.notNull(receiverVO, "receiverVO must be provided.");

        Order order = orderRepository.load(orderId);
        order.changeReceiver(receiverVO);

        orderRepository.save(order);
    }

    public void changePaymentMode(String orderId, String paymentMode) {
        KryAssert.notEmpty(orderId, "orderId must be provided.");
        KryAssert.notEmpty(paymentMode, "paymentMode must be provided.");

        Order order = orderRepository.load(orderId);
        order.changePaymentMode(paymentMode);

        orderRepository.save(order);
    }


    @Transactional(readOnly = true)
    public OrderVO loadOrder(String orderId) {
        Order order = orderRepository.load(orderId);

        List<OrderItemVO> items = new ArrayList<>();
        OrderVO orderVO = new OrderVO();
        orderVO.setOrderId(order.getOrderId()).setItems(items)
                .setCustomerId(order.getCustomer().getId())
                .setCustomerName(order.getCustomer().getName())
                .setDiscountAmount(order.getDiscountAmount())
                .setFinalAmount(order.getFinalAmount())
                .setOriginAmount(order.getOriginAmount())
                .setPaymentMode(order.getPaymentMode().value());

        if (!CollectionUtils.isEmpty(order.getItems())) {
            for (OrderItem item : order.getItems()) {
                OrderItemVO itemVO = new OrderItemVO();
                itemVO.setItemId(item.getItemId()).setPrice(item.getPrice())
                        .setProductId(item.getProductId()).setProductName(item.getProductName())
                        .setQuantity(item.getQuantity());
                items.add(itemVO);
            }
        }

        if (order.getReceiver() != null) {
            ReceiverVO receiverVO = new ReceiverVO();
            receiverVO.setName(order.getReceiver().getName()).setPhoneNo(order.getReceiver().getPhoneNo())
                    .setAddress(new ReceiverVO.Address()
                            .setStreet(order.getReceiver().getAddress().getStreet())
                            .setProvince(order.getReceiver().getAddress().getProvince())
                            .setDistrict(order.getReceiver().getAddress().getDistrict())
                            .setCity(order.getReceiver().getAddress().getCity())
                            .setBuildingAndNumber(order.getReceiver().getAddress().getBuildingAndNumber())
                    );
            orderVO.setReceiver(receiverVO);
        }

        return orderVO;
    }

}
