package com.kry.trade.command.adapter.web.controller;

import com.kry.trade.command.adapter.web.controller.dataobjects.OrderCreationDO;
import com.kry.trade.command.adapter.web.controller.dataobjects.ReceiverDO;
import com.kry.trade.command.application.OrderApplicationService;
import com.kry.trade.command.application.valueobjects.OrderCreationVO;
import com.kry.trade.command.application.valueobjects.OrderItemCreationVO;
import com.kry.trade.command.application.valueobjects.OrderVO;
import com.kry.trade.command.application.valueobjects.ReceiverVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Api(description = "Order management", tags = "100 - Order APIs")
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderApplicationService orderService;


    @ApiOperation(value = "Create a new order.", notes = "Create a new order.")
    @PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public void createOrder(@RequestBody OrderCreationDO order) {
        OrderCreationVO creationVO = new OrderCreationVO();
        order.getItems().forEach(item -> {
            OrderItemCreationVO itemCreationVO = new OrderItemCreationVO();
            itemCreationVO.setProductId(item.getProductId()).setProductName(item.getProductName())
                    .setQuantity(item.getQuantity()).setPrice(item.getPrice());
            creationVO.addItem(itemCreationVO);
        });
        orderService.createOrder(creationVO);
    }

    @ApiOperation(value = "Load order information.", notes = "Load order information.")
    @GetMapping(value = "/{orderId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public OrderVO loadOrder(@PathVariable String orderId) {
        return orderService.loadOrder(orderId);
    }


    @ApiOperation(value = "Confirm order.", notes = "Confirm order.")
    @PutMapping(value = "/{orderId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public void confirm(@PathVariable String orderId) {
        orderService.confirm(orderId);
    }

    @ApiOperation(value = "Cancel order.", notes = "Cancel order.")
    @PatchMapping(value = "/{orderId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public void cancel(@PathVariable String orderId) {
        orderService.cancel(orderId);
    }

    @ApiOperation(value = "Change quantity of order item.", notes = "Change quantity of order item.")
    @PatchMapping(value = "/{orderId}/item/{itemId}/quantity/{quantity}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public void changeQuanity(@PathVariable String orderId, @PathVariable String itemId,
                              @PathVariable int quantity) {
        orderService.changeQuantityOfItem(orderId, itemId, quantity);
    }

    @ApiOperation(value = "Change payment mode.", notes = "Change payment mode.")
    @ApiImplicitParam(name = "payMode", required = true, allowableValues = "OL,OFL")
    @PatchMapping(value = "/{orderId}/paymode/{payMode}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public void changePayMode(@PathVariable String orderId, @PathVariable String payMode) {
        orderService.changePaymentMode(orderId, payMode);
    }

    @ApiOperation(value = "Change receiver.", notes = "Change receiver.")
    @PatchMapping(value = "/{orderId}/receiver", produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public void changeReceiver(@PathVariable String orderId, @RequestBody ReceiverDO receiver) {
        ReceiverVO receiverVO = new ReceiverVO();
        receiverVO.setName(receiver.getName())
                .setPhoneNo(receiver.getPhoneNo())
                .setAddress(
                        new ReceiverVO.Address()
                                .setBuildingAndNumber(receiver.getBuildingAndNumber())
                                .setCity(receiver.getCity())
                                .setDistrict(receiver.getDistrict())
                                .setProvince(receiver.getProvince())
                                .setStreet(receiver.getStreet())
                );
        orderService.changeReceiver(orderId, receiverVO);
    }

}
