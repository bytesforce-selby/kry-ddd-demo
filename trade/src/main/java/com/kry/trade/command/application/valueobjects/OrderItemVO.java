package com.kry.trade.command.application.valueobjects;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Accessors(chain = true)
@Data
public class OrderItemVO extends OrderItemBaseVO {
    private String itemId;
}
