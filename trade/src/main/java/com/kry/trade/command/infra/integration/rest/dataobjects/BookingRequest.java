package com.kry.trade.command.infra.integration.rest.dataobjects;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Accessors(chain = true)
@Data
public class BookingRequest {

    private String sourceId;
    private List<Detail> details;

    @Accessors(chain = true)
    @Data
    public static class Detail {
        private String productId;
        private int quantity;
    }
}
