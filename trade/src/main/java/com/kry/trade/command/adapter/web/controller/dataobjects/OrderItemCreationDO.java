package com.kry.trade.command.adapter.web.controller.dataobjects;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@ApiModel
@Data
public class OrderItemCreationDO {
    @ApiModelProperty(required = true)
    private String productId;
    @ApiModelProperty(required = true)
    private String productName;
    @ApiModelProperty(required = true)
    private Integer quantity;
    @ApiModelProperty(required = true)
    private BigDecimal price;
}
