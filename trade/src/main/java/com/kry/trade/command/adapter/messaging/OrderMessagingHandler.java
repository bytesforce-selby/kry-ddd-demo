package com.kry.trade.command.adapter.messaging;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class OrderMessagingHandler {

    @KafkaListener(topics = "Order.OrderCreatedEvent", groupId = "order_consumer")
    public void handle(ConsumerRecord<String, String> message) {
        String event = message.value();
        log.info(message.topic() + ": " + message.key());
        log.info(event);
    }

}
