package com.kry.trade.command.application.valueobjects;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Accessors(chain = true)
@Data
public class ReceiverVO {

    @NotNull
    private String name;
    @NotNull
    private String phoneNo;
    @NotNull
    private Address address;


    @Accessors(chain = true)
    @Data
    public static class Address {

        @NotNull
        private String province;
        @NotNull
        private String city;
        @NotNull
        private String district;
        @NotNull
        private String street;
        @NotNull
        private String buildingAndNumber;

    }
}
