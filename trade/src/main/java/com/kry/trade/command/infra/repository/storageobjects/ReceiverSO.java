package com.kry.trade.command.infra.repository.storageobjects;

import lombok.Data;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Data
public class ReceiverSO {

    private String name;
    private String phoneNo;

    private String province;
    private String city;
    private String district;
    private String street;
    private String buildingAndNumber;

}