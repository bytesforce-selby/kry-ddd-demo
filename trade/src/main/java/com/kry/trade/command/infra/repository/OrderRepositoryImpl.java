package com.kry.trade.command.infra.repository;

import com.kry.domain.AbstractRepository;
import com.kry.domain.DomainEvent;
import com.kry.exception.AggregateNotFoundException;
import com.kry.infra.repository.DaoProvider;
import com.kry.trade.command.domain.order.*;
import com.kry.trade.command.infra.repository.storageobjects.OrderItemSO;
import com.kry.trade.command.infra.repository.storageobjects.OrderSO;
import com.kry.trade.command.infra.repository.storageobjects.ReceiverSO;
import com.kry.utils.JsonUtils;
import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Responsibility: Store and loadOne aggregate
 * <p>
 * 1. Convert between aggregate and data storage object
 * 2. Persist aggregate
 * 3. Cache should be here as well if necessary
 */
@Repository
public class OrderRepositoryImpl extends AbstractRepository<Order> implements OrderRepository {

    @Autowired
    private DaoProvider daoProvider;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Override
    protected void saveAggregate(Order aggregate) {
        storeOrder(aggregate);
        storeOrderItem(aggregate);
    }

    @Override
    protected void publishEvent(Order aggregate) {
        DomainEvent event = getEvent(aggregate);
        if (event != null) {
            kafkaTemplate.send(getDefaultTopicName(event), aggregate.getEntityId().toString(), JsonUtils.toJson(event));
        }
    }

    private void storeOrder(Order aggregate) {
        OrderSO orderSVO = new OrderSO();
        orderSVO.setOrderId(aggregate.getEntityId().toString())
                .setCustomerId(aggregate.getCustomer().getId()).setCustomerName(aggregate.getCustomer().getName())
                .setDiscountAmount(aggregate.getDiscountAmount()).setOriginAmount(aggregate.getOriginAmount())
                .setFinalAmount(aggregate.getFinalAmount())
                .setPaymentMode(aggregate.getPaymentMode().value())
                .setStatus(aggregate.getStatus().value());

        if (aggregate.getReceiver() != null) {
            ReceiverSO receiverSO = new ReceiverSO();
            receiverSO.setBuildingAndNumber(aggregate.getReceiver().getAddress().getBuildingAndNumber())
                    .setCity(aggregate.getReceiver().getAddress().getCity())
                    .setDistrict(aggregate.getReceiver().getAddress().getDistrict())
                    .setStreet(aggregate.getReceiver().getAddress().getStreet())
                    .setProvince(aggregate.getReceiver().getAddress().getProvince())
                    .setName(aggregate.getReceiver().getName())
                    .setPhoneNo(aggregate.getReceiver().getPhoneNo());
            orderSVO.setReceiver(JsonUtils.toJson(receiverSO));
        }

        daoProvider.save(orderSVO);
    }

    // TODO To improve performance use batch insertion
    private void storeOrderItem(Order aggregate) {
        if (!CollectionUtils.isEmpty(aggregate.getItems())) {
            for (OrderItem item : aggregate.getItems()) {
                OrderItemSO orderItemSO = new OrderItemSO();
                orderItemSO.setItemId(item.getItemId());
                orderItemSO.setOrderId(aggregate.getOrderId());
                orderItemSO.setPrice(item.getPrice());
                orderItemSO.setProductId(item.getProductId());
                orderItemSO.setProductName(item.getProductName());
                orderItemSO.setQuantity(item.getQuantity().byteValue());
                daoProvider.save(orderItemSO);
            }
        }
    }


    @Override
    public Order load(Serializable aggregateId) {
        OrderSO orderSVO = daoProvider.loadOne(OrderSO.class, aggregateId);
        if (orderSVO == null) {
            throw new AggregateNotFoundException(aggregateId.toString());
        }
        List<OrderItemSO> orderItemSVO = daoProvider.loadList(OrderItemSO.class, aggregateId);

        List<OrderItem> items = new ArrayList<>();

        Receiver receiver = null;
        if (!StringUtils.isEmpty(orderSVO.getReceiver())) {
            ReceiverSO receiverSO = JsonUtils.fromJson(ReceiverSO.class, orderSVO.getReceiver());
            receiver = Receiver.builder().name(receiverSO.getName()).phoneNo(receiverSO.getPhoneNo())
                    .address(Receiver.Address.builder()
                            .buildingAndNumber(receiverSO.getBuildingAndNumber())
                            .province(receiverSO.getProvince())
                            .city(receiverSO.getCity())
                            .district(receiverSO.getDistrict())
                            .street(receiverSO.getStreet())
                            .build()
                    ).build();
        }

        Order order = Order.builder().orderId(orderSVO.getOrderId())
                .customer(new Customer(orderSVO.getCustomerId(), orderSVO.getCustomerName()))
                .discountAmount(orderSVO.getDiscountAmount())
                .finalAmount(orderSVO.getFinalAmount())
                .originAmount(orderSVO.getOriginAmount())
                .paymentMode(PaymentMode.from(orderSVO.getPaymentMode()))
                .receiver(receiver)
                .items(items)
                .status(OrderStatus.from(orderSVO.getStatus()))
                .build();

        if (!CollectionUtils.isEmpty(orderItemSVO)) {
            for (OrderItemSO item : orderItemSVO) {
                OrderItem orderItem = OrderItem.builder().itemId(item.getItemId())
                        .price(item.getPrice()).productId(item.getProductId())
                        .productName(item.getProductName()).quantity(item.getQuantity().intValue())
                        .build();
                orderItem.calculateTotalAmount();
                items.add(orderItem);
            }
        }

        return order;
    }

}
