package com.kry.trade.command.application.valueobjects;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Accessors(chain = true)
@Data
public class OrderItemBaseVO {
    private String productId;
    private String productName;
    private Integer quantity;
    private BigDecimal price;
}
