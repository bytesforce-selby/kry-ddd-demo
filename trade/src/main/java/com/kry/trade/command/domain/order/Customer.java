package com.kry.trade.command.domain.order;

import com.kry.domain.ValueObject;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public final class Customer implements ValueObject {

    private String id;
    private String name;

}
