package com.kry.trade.command.exception;

import com.kry.exception.BusinessException;

public class InvalidQuantityException extends BusinessException {

    public InvalidQuantityException(int quantity) {
        super(ErrorCode.INVALID_ITEM_QUANTITY,
                String.format("Quantity of item must be greater than zero, actually value is %s", quantity)
        );
    }
}
