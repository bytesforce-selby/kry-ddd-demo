package com.kry.trade.command.infra.integration.rest.services;

import com.google.common.collect.Lists;
import com.kry.adapter.web.Response;
import com.kry.trade.command.domain.order.OrderDomainService;
import com.kry.trade.command.domain.order.OrderItem;
import com.kry.trade.command.domain.order.Receiver;
import com.kry.trade.command.exception.BookingInventoryException;
import com.kry.trade.command.infra.integration.rest.dataobjects.BookingRequest;
import com.kry.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class OrderRestService implements OrderDomainService {

    @Autowired
    private RestTemplate restTemplate;

    private static final String CUSTOMER_DEFAULT_RECEIVER_URI = "http://localhost:8082/customer/{customerId}/receiver/default";
    private static final String BOOKING_INVENTORY_URI = "http://localhost:8084/inventory/booking";

    @Override
    public Receiver getDefaultReceiver(String customerId) {
        String responseStr = null;
        try {
            responseStr = restTemplate.getForObject(CUSTOMER_DEFAULT_RECEIVER_URI, String.class, customerId);
        } catch (Exception e) {
            log.error(String.format("Failed to invoke service %s", CUSTOMER_DEFAULT_RECEIVER_URI), e);
        }

        if (!StringUtils.isEmpty(responseStr)) {
            Response response = JsonUtils.fromJson(Response.class, responseStr);
            if (response.isSuccess()) {
                Map<String, String> data = (Map<String, String>) response.getData();
                return Receiver.builder().name(data.get("receiverName"))
                        .phoneNo(data.get("receiverMobile"))
                        .address(Receiver.Address.builder().street(data.get("street"))
                                .district(data.get("district"))
                                .city(data.get("city"))
                                .province(data.get("province"))
                                .buildingAndNumber(data.get("buildingAndNumber"))
                                .build()
                        ).build();
            }
        }

        return null;
    }

    @Override
    public void bookInventory(String orderId, List<OrderItem> items) {
        String responseStr;
        try {
            BookingRequest request = new BookingRequest();
            request.setSourceId(orderId);
            List<BookingRequest.Detail> details = Lists.newArrayList();
            request.setDetails(details);

            items.forEach(item -> {
                BookingRequest.Detail detail = new BookingRequest.Detail();
                detail.setProductId(item.getProductId()).setQuantity(item.getQuantity());
                details.add(detail);
            });

            responseStr = restTemplate.patchForObject(BOOKING_INVENTORY_URI, request, String.class);
        } catch (Exception e) {
            log.error(String.format("Failed to invoke service %s", BOOKING_INVENTORY_URI), e);
            throw e;
        }

        if (!StringUtils.isEmpty(responseStr)) {
            Response response = JsonUtils.fromJson(Response.class, responseStr);
            if (!response.isSuccess()) {
                throw new BookingInventoryException(response.getErrorCode(), response.getErrorMessage());
            }
        }

    }

}
