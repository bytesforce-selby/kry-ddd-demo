package com.kry.trade.command.infra.repository.mybatis.mappers;

import com.kry.trade.command.infra.repository.storageobjects.OrderSO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.Map;

@Mapper
public interface OrderMapper {

    /**
     * @mbg.generated Fri May 18 16:24:21 CST 2018
     */
    @Insert({
            "insert into kry_order (order_id, customer_name, ",
            "customer_id, receiver, final_amount, ",
            "discount_amount, origin_amount, pay_mode, ",
            "create_date, create_user, ",
            "update_date, update_user, ",
            "ver, status)",
            "values (#{orderId,jdbcType=VARCHAR}, #{customerName,jdbcType=VARCHAR}, ",
            "#{customerId,jdbcType=VARCHAR}, #{receiver,jdbcType=VARCHAR}, #{finalAmount,jdbcType=DECIMAL}, ",
            "#{discountAmount,jdbcType=DECIMAL}, #{originAmount,jdbcType=DECIMAL}, #{paymentMode,jdbcType=VARCHAR},",
            "#{createDate,jdbcType=TIMESTAMP}, #{createUser,jdbcType=VARCHAR}, ",
            "#{updateDate,jdbcType=TIMESTAMP}, #{updateUser,jdbcType=VARCHAR}, ",
            "#{ver,jdbcType=TINYINT}, #{status,jdbcType=VARCHAR})"
    })
    int insert(OrderSO record);

    /**
     * @mbg.generated Fri May 18 16:24:21 CST 2018
     */
    @Select({
            "select",
            "order_id, customer_name, customer_id, receiver, final_amount, discount_amount, origin_amount, pay_mode, ",
            "create_date, create_user, update_date, update_user, ver, status ",
            "from kry_order",
            "where order_id = #{orderId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column = "order_id", property = "orderId", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "customer_name", property = "customerName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "customer_id", property = "customerId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "receiver", property = "receiver", jdbcType = JdbcType.VARCHAR),
            @Result(column = "status", property = "status", jdbcType = JdbcType.VARCHAR),
            @Result(column = "pay_mode", property = "paymentMode", jdbcType = JdbcType.VARCHAR),
            @Result(column = "final_amount", property = "finalAmount", jdbcType = JdbcType.DECIMAL),
            @Result(column = "discount_amount", property = "discountAmount", jdbcType = JdbcType.DECIMAL),
            @Result(column = "origin_amount", property = "originAmount", jdbcType = JdbcType.DECIMAL),
            @Result(column = "create_date", property = "createDate", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "create_user", property = "createUser", jdbcType = JdbcType.VARCHAR),
            @Result(column = "update_date", property = "updateDate", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_user", property = "updateUser", jdbcType = JdbcType.VARCHAR),
            @Result(column = "ver", property = "ver", jdbcType = JdbcType.TINYINT)
    })
    OrderSO selectByPrimaryKey(String orderId);

    /**
     * @mbg.generated Fri May 18 16:24:21 CST 2018
     */
    @Update({
            "update kry_order",
            "set customer_name = #{entity.customerName,jdbcType=VARCHAR},",
            "customer_id = #{entity.customerId,jdbcType=VARCHAR},",
            "receiver = #{entity.receiver,jdbcType=VARCHAR},",
            "status = #{entity.status,jdbcType=VARCHAR},",
            "pay_mode = #{entity.paymentMode,jdbcType=VARCHAR},",
            "final_amount = #{entity.finalAmount,jdbcType=DECIMAL},",
            "discount_amount = #{entity.discountAmount,jdbcType=DECIMAL},",
            "origin_amount = #{entity.originAmount,jdbcType=DECIMAL},",
            "create_date = #{entity.createDate,jdbcType=TIMESTAMP},",
            "create_user = #{entity.createUser,jdbcType=VARCHAR},",
            "update_date = #{entity.updateDate,jdbcType=TIMESTAMP},",
            "update_user = #{entity.updateUser,jdbcType=VARCHAR},",
            "ver = #{entity.ver,jdbcType=TINYINT}",
            "where order_id = #{entity.orderId,jdbcType=VARCHAR} and ver = #{ver,jdbcType=TINYINT}"
    })
    int updateByPrimaryKey(Map<String, Object> params);

}