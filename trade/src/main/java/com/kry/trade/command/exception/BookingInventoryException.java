package com.kry.trade.command.exception;

import com.kry.exception.BusinessException;

public class BookingInventoryException extends BusinessException {

    public BookingInventoryException(String errorCode, String errorMessage) {
        super(errorCode, errorMessage);
    }

    public BookingInventoryException(String errorCode) {
        super(errorCode);
    }
}
