package com.kry.trade.command.infra.repository.storageobjects;

import com.kry.infra.repository.mybatis.annotation.StorageMapper;
import com.kry.infra.repository.BaseSO;
import com.kry.trade.command.infra.repository.mybatis.mappers.OrderItemMapper;

import java.io.Serializable;
import java.math.BigDecimal;

@StorageMapper(OrderItemMapper.class)
public class OrderItemSO extends BaseSO implements Serializable {

    private String itemId;

    private String orderId;

    private String productId;

    private String productName;

    private Byte quantity;

    private BigDecimal price;

    private static final long serialVersionUID = 1L;

    public String getItemId() {
        return itemId;
    }

    public OrderItemSO withItemId(String itemId) {
        this.setItemId(itemId);
        return this;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getOrderId() {
        return orderId;
    }

    public OrderItemSO withOrderId(String orderId) {
        this.setOrderId(orderId);
        return this;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductId() {
        return productId;
    }

    public OrderItemSO withProductId(String productId) {
        this.setProductId(productId);
        return this;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public OrderItemSO withProductName(String productName) {
        this.setProductName(productName);
        return this;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Byte getQuantity() {
        return quantity;
    }

    public OrderItemSO withQuantity(Byte quantity) {
        this.setQuantity(quantity);
        return this;
    }

    public void setQuantity(Byte quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public OrderItemSO withPrice(BigDecimal price) {
        this.setPrice(price);
        return this;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", itemId=").append(itemId);
        sb.append(", orderId=").append(orderId);
        sb.append(", productId=").append(productId);
        sb.append(", productName=").append(productName);
        sb.append(", quantity=").append(quantity);
        sb.append(", price=").append(price);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public String getPrimaryKeyValue() {
        return this.itemId;
    }
}