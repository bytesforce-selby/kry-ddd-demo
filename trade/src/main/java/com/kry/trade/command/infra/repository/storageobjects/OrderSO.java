package com.kry.trade.command.infra.repository.storageobjects;

import com.kry.infra.repository.BaseSO;
import com.kry.infra.repository.mybatis.annotation.StorageMapper;
import com.kry.trade.command.infra.repository.mybatis.mappers.OrderMapper;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

@StorageMapper(OrderMapper.class)
@Accessors(chain = true)
@Data
public class OrderSO extends BaseSO implements Serializable {

    private String orderId;

    private String customerId;

    private String customerName;

    private BigDecimal finalAmount;

    private BigDecimal discountAmount;

    private BigDecimal originAmount;

    private String paymentMode;

    private String receiver;

    private String status;

    @Override
    public Serializable getPrimaryKeyValue() {
        return orderId;
    }


}