package com.kry.trade.command.domain.order;

import com.kry.domain.ValueObject;
import com.kry.trade.command.application.valueobjects.ReceiverVO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
public class Receiver implements ValueObject {

    private String name;
    private String phoneNo;
    private Address address;

    @Builder
    @Getter
    public static class Address implements ValueObject {

        private String province;
        private String city;
        private String district;
        private String street;
        private String buildingAndNumber;

    }

    public static Receiver create(ReceiverVO receiverVO) {
        Receiver receiver = Receiver.builder().name(receiverVO.getName()).phoneNo(receiverVO.getPhoneNo())
                .address(Address.builder()
                        .province(receiverVO.getAddress().getProvince())
                        .city(receiverVO.getAddress().getCity())
                        .district(receiverVO.getAddress().getDistrict())
                        .street(receiverVO.getAddress().getStreet())
                        .buildingAndNumber(receiverVO.getAddress().getBuildingAndNumber()).build()
                ).build();
        return receiver;
    }
}