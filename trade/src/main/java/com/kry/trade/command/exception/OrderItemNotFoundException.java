package com.kry.trade.command.exception;

import com.kry.exception.BusinessException;

public class OrderItemNotFoundException extends BusinessException {

    public OrderItemNotFoundException(String itemId) {
        super(ErrorCode.ITEM_NOT_FOUND,
                String.format("Order item not found with item id [%s]", itemId)
        );
    }
}
