package com.kry.trade.command.adapter.web.controller.dataobjects;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@ApiModel
@Accessors(chain = true)
@Data
public class ReceiverDO {

    @ApiModelProperty(required = true)
    private String name;
    @ApiModelProperty(required = true)
    private String phoneNo;
    @ApiModelProperty(required = true)
    private String province;
    @ApiModelProperty(required = true)
    private String city;
    @ApiModelProperty(required = true)
    private String district;
    @ApiModelProperty(required = true)
    private String street;
    @ApiModelProperty(required = true)
    private String buildingAndNumber;

}
