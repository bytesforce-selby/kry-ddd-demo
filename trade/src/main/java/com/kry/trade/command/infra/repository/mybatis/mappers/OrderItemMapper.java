package com.kry.trade.command.infra.repository.mybatis.mappers;

import com.kry.trade.command.infra.repository.storageobjects.OrderItemSO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;
import java.util.Map;

@Mapper
public interface OrderItemMapper {

    @Insert({
        "insert into kry_order_item (item_id, order_id, ",
        "product_id, product_name, ",
        "quantity, price, ",
        "create_date, create_user, ",
        "update_date, update_user, ",
        "ver)",
        "values (#{itemId,jdbcType=VARCHAR}, #{orderId,jdbcType=VARCHAR}, ",
        "#{productId,jdbcType=VARCHAR}, #{productName,jdbcType=VARCHAR}, ",
        "#{quantity,jdbcType=TINYINT}, #{price,jdbcType=DECIMAL}, ",
        "#{createDate,jdbcType=TIMESTAMP}, #{createUser,jdbcType=VARCHAR}, ",
        "#{updateDate,jdbcType=TIMESTAMP}, #{updateUser,jdbcType=VARCHAR}, ",
        "#{ver,jdbcType=TINYINT})"
    })
    int insert(OrderItemSO record);

    @Select({
        "select",
        "item_id, order_id, product_id, product_name, quantity, price, create_date, create_user, ",
        "update_date, update_user, ver",
        "from kry_order_item",
        "where item_id = #{itemId,jdbcType=VARCHAR}"
    })
    @Results({
        @Result(column="item_id", property="itemId", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="order_id", property="orderId", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_name", property="productName", jdbcType=JdbcType.VARCHAR),
        @Result(column="quantity", property="quantity", jdbcType=JdbcType.TINYINT),
        @Result(column="price", property="price", jdbcType=JdbcType.DECIMAL),
        @Result(column="create_date", property="createDate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="create_user", property="createUser", jdbcType=JdbcType.VARCHAR),
        @Result(column="update_date", property="updateDate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_user", property="updateUser", jdbcType=JdbcType.VARCHAR),
        @Result(column="ver", property="ver", jdbcType=JdbcType.TINYINT)
    })
    OrderItemSO selectByPrimaryKey(String itemId);


    @Select({
            "select",
            "item_id, order_id, product_id, product_name, quantity, price, create_date, create_user, ",
            "update_date, update_user, ver",
            "from kry_order_item",
            "where order_id = #{order_id,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column="item_id", property="itemId", jdbcType=JdbcType.VARCHAR, id=true),
            @Result(column="order_id", property="orderId", jdbcType=JdbcType.VARCHAR),
            @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
            @Result(column="product_name", property="productName", jdbcType=JdbcType.VARCHAR),
            @Result(column="quantity", property="quantity", jdbcType=JdbcType.TINYINT),
            @Result(column="price", property="price", jdbcType=JdbcType.DECIMAL),
            @Result(column="create_date", property="createDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="create_user", property="createUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="update_date", property="updateDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="update_user", property="updateUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="ver", property="ver", jdbcType=JdbcType.TINYINT)
    })
    List<OrderItemSO> selectByAggregateId(String orderId);


    @Update({
        "update kry_order_item",
        "set order_id = #{entity.orderId,jdbcType=VARCHAR},",
          "product_id = #{entity.productId,jdbcType=VARCHAR},",
          "product_name = #{entity.productName,jdbcType=VARCHAR},",
          "quantity = #{entity.quantity,jdbcType=TINYINT},",
          "price = #{entity.price,jdbcType=DECIMAL},",
          "create_date = #{entity.createDate,jdbcType=TIMESTAMP},",
          "create_user = #{entity.createUser,jdbcType=VARCHAR},",
          "update_date = #{entity.updateDate,jdbcType=TIMESTAMP},",
          "update_user = #{entity.updateUser,jdbcType=VARCHAR},",
          "ver = #{entity.ver,jdbcType=TINYINT}",
        "where item_id = #{entity.itemId,jdbcType=VARCHAR} and ver = #{ver,jdbcType=TINYINT}"
    })
    int updateByPrimaryKey(Map<String, Object> params);
}