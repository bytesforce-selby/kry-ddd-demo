package com.kry.trade.command.exception;

public final class ErrorCode {
    public static final String ILLEGAL_ORDER_OPERATION = "80001";
    public static final String INVALID_ITEM_QUANTITY = "80002";
    public static final String ITEM_NOT_FOUND = "80003";
}
