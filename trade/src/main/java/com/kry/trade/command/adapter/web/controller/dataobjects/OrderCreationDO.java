package com.kry.trade.command.adapter.web.controller.dataobjects;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@ApiModel
@Data
public class OrderCreationDO {

    @ApiModelProperty(required = true, notes = "Order items")
    private List<OrderItemCreationDO> items;

}