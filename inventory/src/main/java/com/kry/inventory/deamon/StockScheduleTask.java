package com.kry.inventory.deamon;

import com.kry.inventory.command.application.StockApplicationService;
import com.kry.inventory.query.dao.StockDAO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;


@Slf4j
@EnableScheduling
@Component
public class StockScheduleTask {

    @Autowired
    private StockApplicationService stockService;
    @Autowired
    private StockDAO stockDAO;

    @Scheduled(fixedDelay = 1)
    public void expireBooking() {
        List<String> results = stockDAO.findAllExpiredBooking();
        if (!CollectionUtils.isEmpty(results)) {
            for (String bookingId : results) {
                stockService.expireBooking(bookingId);
            }
        }
    }

}
