package com.kry.inventory.query.dao.impl.mybatis;

import com.kry.inventory.query.dao.StockDAO;
import com.kry.inventory.query.dao.impl.mybatis.mapper.StockQueryMapper;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StockDAOImpl implements StockDAO {

    @Autowired
    private StockQueryMapper queryMapper;

    @Override
    public List<String> findAllExpiredBooking() {
        return queryMapper.findAllExpiredBooking(DateTime.now().toDate());
    }

}
