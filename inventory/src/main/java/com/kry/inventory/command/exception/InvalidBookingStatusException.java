package com.kry.inventory.command.exception;

import com.kry.exception.BusinessException;

public class InvalidBookingStatusException extends BusinessException {

    public InvalidBookingStatusException(String message) {
        super(ErrorCode.STOCK_BOOKING_STATUS_INVALID, message);
    }

}
