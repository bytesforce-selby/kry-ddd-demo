package com.kry.inventory.command.exception;

public final class ErrorCode {

    public static final String STOCK_NOT_ENOUGH = "60001";

    public static final String STOCK_LESS_THAN_OCCUPIED = "60002";

    public static final String STOCK_BOOKING_DUPLICATED = "60003";

    public static final String STOCK_BOOKING_STATUS_INVALID = "60004";

}
