package com.kry.inventory.command.domain.stockchange;

import com.kry.domain.Aggregate;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

@Builder
@Getter
public class StockChange extends Aggregate {

    private String id;
    private StockId stockId;

    private ChangeSource changeSource;
    private int quantity;
    private boolean isOut;

    @Override
    public Serializable getAggregateId() {
        return null;
    }
}
