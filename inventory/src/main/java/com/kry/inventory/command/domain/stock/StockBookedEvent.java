package com.kry.inventory.command.domain.stock;

import com.kry.domain.BaseDomainEvent;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class StockBookedEvent extends BaseDomainEvent {

    private String sourceId;
    private String stockId;
    private int quantity;

}
