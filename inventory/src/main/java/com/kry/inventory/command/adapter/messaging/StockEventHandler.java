package com.kry.inventory.command.adapter.messaging;

import com.kry.inventory.command.adapter.messaging.dataobjects.BookingCompletedEventDO;
import com.kry.inventory.command.adapter.messaging.dataobjects.BookingExpiredEventDO;
import com.kry.inventory.command.adapter.messaging.dataobjects.StockBookedEventDO;
import com.kry.inventory.command.application.StockApplicationService;
import com.kry.inventory.command.application.valueobjects.BookingCompletedVO;
import com.kry.inventory.command.application.valueobjects.BookingCreationVO;
import com.kry.inventory.command.application.valueobjects.BookingExpiredVO;
import com.kry.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class StockEventHandler {

    @Autowired
    private StockApplicationService stockService;

    @KafkaListener(topics = "Inventory.StockBookedEvent")
    public void handleBookedEvent(Consumer<?, ?> consumer, ConsumerRecord<String, String> record) {
        try {
            StockBookedEventDO eventVO = JsonUtils.fromJson(StockBookedEventDO.class, record.value());

            BookingCreationVO creationVO = new BookingCreationVO();
            creationVO.setSourceId(eventVO.getSourceId()).setProductId(eventVO.getStockId())
                    .setQuantity(eventVO.getQuantity());

            stockService.createStockBooking(creationVO);

            consumer.commitAsync();
        } catch (Exception e) {
            log.error("Failed to consumer stock booked event", e);
        }
    }


    @KafkaListener(topics = "Inventory.BookingExpiredEvent")
    public void handleBookingExpiredEvent(Consumer<?, ?> consumer, ConsumerRecord<String, String> record) {
        try {
            BookingExpiredEventDO eventVO = JsonUtils.fromJson(BookingExpiredEventDO.class, record.value());

            BookingExpiredVO expiredVO = new BookingExpiredVO();
            expiredVO.setStockId(eventVO.getStockId()).setQuantity(eventVO.getQuantity());

            stockService.releaseBooking(expiredVO);

            consumer.commitAsync();
        } catch (Exception e) {
            log.error("Failed to consumer stock booking expired event", e);
        }
    }

    @KafkaListener(topics = "Inventory.BookingCompletedEvent")
    public void handleBookingCompletedEvent(Consumer<?, ?> consumer, ConsumerRecord<String, String> record) {
        try {
            BookingCompletedEventDO eventVO = JsonUtils.fromJson(BookingCompletedEventDO.class, record.value());

            BookingCompletedVO completedVO = new BookingCompletedVO();
            completedVO.setStockId(eventVO.getStockId()).setQuantity(eventVO.getQuantity());

            stockService.commitBooking(completedVO);

            consumer.commitAsync();
        } catch (Exception e) {
            log.error("Failed to consumer stock booking completed event", e);
        }
    }

}
