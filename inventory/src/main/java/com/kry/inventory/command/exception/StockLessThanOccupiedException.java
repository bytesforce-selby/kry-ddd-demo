package com.kry.inventory.command.exception;

import com.kry.exception.BusinessException;

public class StockLessThanOccupiedException extends BusinessException {

    public StockLessThanOccupiedException() {
    }

    public StockLessThanOccupiedException(String message) {
        super(ErrorCode.STOCK_LESS_THAN_OCCUPIED, message);
    }

}
