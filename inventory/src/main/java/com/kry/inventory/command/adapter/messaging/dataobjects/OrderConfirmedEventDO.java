package com.kry.inventory.command.adapter.messaging.dataobjects;

import lombok.Data;

@Data
public class OrderConfirmedEventDO {

    private String orderId;
    private String status;

}
