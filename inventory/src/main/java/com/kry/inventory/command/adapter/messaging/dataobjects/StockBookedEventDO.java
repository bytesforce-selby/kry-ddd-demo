package com.kry.inventory.command.adapter.messaging.dataobjects;

import lombok.Data;

@Data
public class StockBookedEventDO {

    private String sourceId;
    private String stockId;
    private int quantity;

}
