package com.kry.inventory.command.adapter.messaging;

import com.kry.inventory.command.adapter.messaging.dataobjects.ProductCreatedEventDO;
import com.kry.inventory.command.application.StockApplicationService;
import com.kry.inventory.command.application.valueobjects.StockCreationVO;
import com.kry.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ProductEventHandler {

    @Autowired
    private StockApplicationService stockService;

    @KafkaListener(topics = "Product.ProductCreatedEvent")
    public void handleCreatedEvent(Consumer<?, ?> consumer, ConsumerRecord<String, String> record) {
        try {

            log.info(record.value());

            ProductCreatedEventDO createdEventDO = JsonUtils.fromJson(ProductCreatedEventDO.class, record.value());

            StockCreationVO creationVO = new StockCreationVO();
            creationVO.setProductId(createdEventDO.getProductId()).setProductName(createdEventDO.getProductName());

            stockService.createStock(creationVO);

            consumer.commitAsync();
        } catch (Exception e) {
            log.error("Failed to consumer product created event", e);
        }
    }

}
