package com.kry.inventory.command.domain.stockchange;

import com.kry.domain.Repository;

public interface StockChangeRepository extends Repository<StockChange> {
}
