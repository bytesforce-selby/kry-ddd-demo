package com.kry.inventory.command.infra.repository.stock.mybatis.mapper;

import com.kry.inventory.command.infra.repository.stock.storageobjects.StockSO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.Map;

@Mapper
public interface StockMapper {

    @Insert({
            "insert into stock (stock_id, product_name, ",
            "stock_cnt, locked_cnt, ",
            "booked_cnt, available_cnt, ",
            "create_date, create_user, ",
            "update_date, update_user, ",
            "ver)",
            "values (#{stockId,jdbcType=VARCHAR}, #{productName,jdbcType=VARCHAR}, ",
            "#{stockCnt,jdbcType=INTEGER}, #{lockedCnt,jdbcType=INTEGER}, ",
            "#{bookedCnt,jdbcType=INTEGER}, #{availableCnt,jdbcType=INTEGER}, ",
            "#{createDate,jdbcType=TIMESTAMP}, #{createUser,jdbcType=VARCHAR}, ",
            "#{updateDate,jdbcType=TIMESTAMP}, #{updateUser,jdbcType=VARCHAR}, ",
            "#{ver,jdbcType=TINYINT})"
    })
    int insert(StockSO record);

    @Select({
            "select",
            "stock_id, product_name, stock_cnt, locked_cnt, booked_cnt, available_cnt, create_date, ",
            "create_user, update_date, update_user, ver",
            "from stock",
            "where stock_id = #{stockId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column="stock_id", property="stockId", jdbcType=JdbcType.VARCHAR, id=true),
            @Result(column="product_name", property="productName", jdbcType=JdbcType.VARCHAR),
            @Result(column="stock_cnt", property="stockCnt", jdbcType=JdbcType.INTEGER),
            @Result(column="locked_cnt", property="lockedCnt", jdbcType=JdbcType.INTEGER),
            @Result(column="booked_cnt", property="bookedCnt", jdbcType=JdbcType.INTEGER),
            @Result(column="available_cnt", property="availableCnt", jdbcType=JdbcType.INTEGER),
            @Result(column="create_date", property="createDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="create_user", property="createUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="update_date", property="updateDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="update_user", property="updateUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="ver", property="ver", jdbcType=JdbcType.TINYINT)
    })
    StockSO selectByPrimaryKey(String stockId);

    @Update({
            "update stock",
            "set product_name = #{entity.productName,jdbcType=VARCHAR},",
            "stock_cnt = #{entity.stockCnt,jdbcType=INTEGER},",
            "locked_cnt = #{entity.lockedCnt,jdbcType=INTEGER},",
            "booked_cnt = #{entity.bookedCnt,jdbcType=INTEGER},",
            "available_cnt = #{entity.availableCnt,jdbcType=INTEGER},",
            "create_date = #{entity.createDate,jdbcType=TIMESTAMP},",
            "create_user = #{entity.createUser,jdbcType=VARCHAR},",
            "update_date = #{entity.updateDate,jdbcType=TIMESTAMP},",
            "update_user = #{entity.updateUser,jdbcType=VARCHAR},",
            "ver = #{entity.ver,jdbcType=TINYINT}",
            "where stock_id = #{entity.stockId,jdbcType=VARCHAR} and ver = #{ver,jdbcType=TINYINT}"
    })
    int updateByPrimaryKey(Map<String, Object> params);

}
