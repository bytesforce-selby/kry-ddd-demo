package com.kry.inventory.command.adapter.messaging;

import com.kry.inventory.command.adapter.messaging.dataobjects.OrderConfirmedEventDO;
import com.kry.inventory.command.application.StockApplicationService;
import com.kry.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class OrderEventHandler {

    @Autowired
    private StockApplicationService stockService;

    private static final String ORDER_WAITING_FOR_SHIPPING = "S";

    @KafkaListener(topics = "Order.OrderConfirmedEvent")
    public void handleConfirmedEvent(Consumer<?,?> consumer, ConsumerRecord<String, String> record) {
        try {
            OrderConfirmedEventDO confirmedEventDO = JsonUtils.fromJson(OrderConfirmedEventDO.class, record.value());
            if (ORDER_WAITING_FOR_SHIPPING.equals(confirmedEventDO.getStatus())) {
                stockService.completeBooking(confirmedEventDO.getOrderId());
            }
            consumer.commitAsync();
        } catch (Exception e) {
            log.error("Failed to consumer order placed event", e);
        }
    }

}
