package com.kry.inventory.command.domain.stock;

import com.kry.domain.Aggregate;
import com.kry.inventory.command.domain.stockbooking.StockBooking;
import com.kry.inventory.command.domain.stockbooking.StockBookingRepository;
import com.kry.inventory.command.exception.DuplicatedBookingException;
import com.kry.inventory.command.exception.StockLessThanOccupiedException;
import com.kry.inventory.command.exception.StockNotEnoughException;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

@Builder
@Getter
public class Stock extends Aggregate {

    private ProductId productId;
    private String productName;

    private int stockCnt;
    private int lockedCnt;
    private int availableCnt;
    private int bookedCnt;

    public void create() {
    }

    public void bookFor(String sourceId, int quantity) {

        StockBooking booked = getBean(StockBookingRepository.class).loadWith(sourceId, productId.getId());
        if (booked != null) {
            throw new DuplicatedBookingException();
        }

        if (availableCnt > quantity) {
            bookedCnt = bookedCnt + quantity;
            reCalcAvailableCnt();

            applyEvent(StockBookedEvent.builder().sourceId(sourceId).stockId(productId.getId())
                    .quantity(quantity).build());

        } else {
            throw new StockNotEnoughException(String.format("Expected %s, actually %s.", quantity, availableCnt));
        }
    }

    public void releaseBooking(int bookedQuantity) {
        bookedCnt = bookedCnt - bookedQuantity;
        reCalcAvailableCnt();
    }

    public void commitBooking(int bookedQuantity) {
        lockedCnt = lockedCnt + bookedQuantity;
        bookedCnt = bookedCnt - bookedQuantity;
        reCalcAvailableCnt();
    }

    private void reCalcAvailableCnt() {
        availableCnt = stockCnt - lockedCnt - bookedCnt;
    }

    public void add(int quantity) {
        stockCnt = stockCnt + quantity;
        reCalcAvailableCnt();
    }

    public void subtract(int quantity) {
        stockCnt = stockCnt - quantity;
        reCalcAvailableCnt();
    }

    public void changeStockQuantity(int quantity) {
        int occupied = bookedCnt + lockedCnt;
        if (quantity < occupied) {
            throw new StockLessThanOccupiedException(String.format("Expected %s, actually occupied %s.", quantity, occupied));
        }
        stockCnt = quantity;
        reCalcAvailableCnt();
    }

    @Override
    public Serializable getAggregateId() {
        return productId.getId();
    }
}
