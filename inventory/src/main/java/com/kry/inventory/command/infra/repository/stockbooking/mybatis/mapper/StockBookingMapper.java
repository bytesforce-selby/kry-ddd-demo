package com.kry.inventory.command.infra.repository.stockbooking.mybatis.mapper;

import com.kry.inventory.command.infra.repository.stockbooking.storageobjects.StockBookingSO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;
import java.util.Map;

@Mapper
public interface StockBookingMapper {

    @Insert({
            "insert into stock_booking (source_id, stock_id, product_id, ",
            "quantity, expire_date, ",
            "status, create_date, ",
            "create_user, update_date, ",
            "update_user, ver)",
            "values (#{sourceId,jdbcType=VARCHAR}, #{stockId,jdbcType=VARCHAR}, #{productId,jdbcType=VARCHAR}, ",
            "#{quantity,jdbcType=INTEGER}, #{expireDate,jdbcType=TIMESTAMP}, ",
            "#{status,jdbcType=TINYINT}, #{createDate,jdbcType=TIMESTAMP}, ",
            "#{createUser,jdbcType=VARCHAR}, #{updateDate,jdbcType=TIMESTAMP}, ",
            "#{updateUser,jdbcType=VARCHAR}, #{ver,jdbcType=TINYINT})"
    })
    int insert(StockBookingSO record);

    @Select({
            "select",
            "source_id, product_id, stock_id, quantity, expire_date, status, create_date, create_user, ",
            "update_date, update_user, ver",
            "from stock_booking",
            "where stock_id = #{stockId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column="stock_id", property="stockId", jdbcType=JdbcType.VARCHAR, id=true),
            @Result(column="source_id", property="sourceId", jdbcType=JdbcType.VARCHAR),
            @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
            @Result(column="quantity", property="quantity", jdbcType=JdbcType.INTEGER),
            @Result(column="expire_date", property="expireDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="status", property="status", jdbcType=JdbcType.TINYINT),
            @Result(column="create_date", property="createDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="create_user", property="createUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="update_date", property="updateDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="update_user", property="updateUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="ver", property="ver", jdbcType=JdbcType.TINYINT)
    })
    StockBookingSO selectByPrimaryKey(String stockId);


    @Select({
            "select",
            "source_id, product_id, stock_id, quantity, expire_date, status, create_date, create_user, ",
            "update_date, update_user, ver",
            "from stock_booking",
            "where source_id = #{sourceId,jdbcType=VARCHAR} and product_id = #{productId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column="stock_id", property="stockId", jdbcType=JdbcType.VARCHAR, id=true),
            @Result(column="source_id", property="sourceId", jdbcType=JdbcType.VARCHAR),
            @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
            @Result(column="quantity", property="quantity", jdbcType=JdbcType.INTEGER),
            @Result(column="expire_date", property="expireDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="status", property="status", jdbcType=JdbcType.TINYINT),
            @Result(column="create_date", property="createDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="create_user", property="createUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="update_date", property="updateDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="update_user", property="updateUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="ver", property="ver", jdbcType=JdbcType.TINYINT)
    })
    StockBookingSO selectWith(@Param("sourceId") String sourceId, @Param("productId")String productId);

    @Select({
            "select",
            "source_id, product_id, stock_id, quantity, expire_date, status, create_date, create_user, ",
            "update_date, update_user, ver",
            "from stock_booking",
            "where source_id = #{sourceId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column="stock_id", property="stockId", jdbcType=JdbcType.VARCHAR, id=true),
            @Result(column="source_id", property="sourceId", jdbcType=JdbcType.VARCHAR),
            @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
            @Result(column="quantity", property="quantity", jdbcType=JdbcType.INTEGER),
            @Result(column="expire_date", property="expireDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="status", property="status", jdbcType=JdbcType.TINYINT),
            @Result(column="create_date", property="createDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="create_user", property="createUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="update_date", property="updateDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="update_user", property="updateUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="ver", property="ver", jdbcType=JdbcType.TINYINT)
    })
    List<StockBookingSO> selectBySource(String sourceId);


    @Update({
            "update stock_booking",
            "set source_id = #{entity.sourceId,jdbcType=VARCHAR},",
            "product_id = #{entity.productId,jdbcType=VARCHAR},",
            "quantity = #{entity.quantity,jdbcType=INTEGER},",
            "expire_date = #{entity.expireDate,jdbcType=TIMESTAMP},",
            "status = #{entity.status,jdbcType=TINYINT},",
            "create_date = #{entity.createDate,jdbcType=TIMESTAMP},",
            "create_user = #{entity.createUser,jdbcType=VARCHAR},",
            "update_date = #{entity.updateDate,jdbcType=TIMESTAMP},",
            "update_user = #{entity.updateUser,jdbcType=VARCHAR},",
            "ver = #{entity.ver,jdbcType=TINYINT}",
            "where stock_id = #{entity.stockId,jdbcType=VARCHAR} and ver = #{ver,jdbcType=TINYINT}"
    })
    int updateByPrimaryKey(Map<String, Object> params);
}
