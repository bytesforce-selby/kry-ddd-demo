package com.kry.inventory.command.infra.repository.stock;

import com.kry.domain.AbstractRepository;
import com.kry.domain.DomainEvent;
import com.kry.exception.AggregateNotFoundException;
import com.kry.infra.repository.DaoProvider;
import com.kry.inventory.command.domain.stock.ProductId;
import com.kry.inventory.command.domain.stock.Stock;
import com.kry.inventory.command.domain.stock.StockRepository;
import com.kry.inventory.command.infra.repository.stock.storageobjects.StockSO;
import com.kry.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public class StockRepositoryImpl extends AbstractRepository<Stock> implements StockRepository {

    @Autowired
    private DaoProvider daoProvider;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Override
    protected void saveAggregate(Stock aggregate) {
        StockSO stockSO = new StockSO();
        stockSO.setStockId(aggregate.getProductId().getId()).setProductName(aggregate.getProductName())
                .setAvailableCnt(aggregate.getAvailableCnt()).setBookedCnt(aggregate.getBookedCnt())
                .setLockedCnt(aggregate.getLockedCnt()).setStockCnt(aggregate.getStockCnt());
        daoProvider.save(stockSO);
    }

    @Override
    protected void publishEvent(Stock aggregate) {
        DomainEvent event = getEvent(aggregate);
        if (event != null) {
            kafkaTemplate.send(getDefaultTopicName(event), aggregate.getAggregateId().toString(), JsonUtils.toJson(event));
        }
    }

    @Override
    public Stock load(Serializable aggregateId) {
        StockSO stockSO = daoProvider.loadOne(StockSO.class, aggregateId);
        if (stockSO == null) {
            throw new AggregateNotFoundException();
        }

        Stock stock = Stock.builder().productId(new ProductId(stockSO.getStockId())).productName(stockSO.getProductName())
                .stockCnt(stockSO.getStockCnt()).availableCnt(stockSO.getAvailableCnt())
                .bookedCnt(stockSO.getBookedCnt()).lockedCnt(stockSO.getLockedCnt())
                .build();

        return stock;
    }
}
