package com.kry.inventory.command.domain.stock;

import com.kry.domain.ValueObject;
import lombok.Getter;

@Getter
public class ProductId implements ValueObject {

    private String id;

    public ProductId(String id) {
        this.id = id;
    }
}
