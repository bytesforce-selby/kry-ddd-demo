package com.kry.inventory.command.domain.stockbooking;

import com.kry.domain.BaseDomainEvent;
import lombok.Getter;

@Getter
public class BookingExpiredEvent extends BaseDomainEvent {

    private String stockId;
    private int quantity;

    public BookingExpiredEvent(String stockId, int quantity) {
        this.stockId = stockId;
        this.quantity = quantity;
    }

}
