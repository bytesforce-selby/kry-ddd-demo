package com.kry.inventory.command.domain.stockchange;

import com.kry.domain.ValueObject;
import lombok.Getter;

@Getter
public class ChangeSource implements ValueObject {

    private SourceEnum source;
    private String sourceId;

    public ChangeSource(SourceEnum source, String sourceId) {
        this.source = source;
        this.sourceId = sourceId;
    }
}
