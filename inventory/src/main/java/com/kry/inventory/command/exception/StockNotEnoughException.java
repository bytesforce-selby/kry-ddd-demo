package com.kry.inventory.command.exception;

import com.kry.exception.BusinessException;

public class StockNotEnoughException extends BusinessException {

    public StockNotEnoughException() {
    }

    public StockNotEnoughException(String message) {
        super(ErrorCode.STOCK_NOT_ENOUGH, message);
    }

}
