package com.kry.inventory.command.domain.stockbooking;

import com.kry.domain.ValueObject;
import com.kry.inventory.command.domain.stock.ProductId;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class BookingDetail implements ValueObject {

    private String sourceId;
    private int quantity;
    private ProductId productId;

}
