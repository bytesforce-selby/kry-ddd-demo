package com.kry.inventory.command.adapter.web.controller;

import com.google.common.collect.Lists;
import com.kry.inventory.command.adapter.web.controller.dataobjects.BookingDO;
import com.kry.inventory.command.application.StockApplicationService;
import com.kry.inventory.command.application.valueobjects.BookingVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(description = "Inventory management", tags = "100 - Inventory APIs")
@RestController
@RequestMapping("/inventory")
public class StockController {

    @Autowired
    private StockApplicationService stockService;


    @ApiOperation(value = "Book stock", notes = "Book stock first before subtracting stock.")
    @ResponseBody
    @PatchMapping(value = "/booking", produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void book(@RequestBody BookingDO booking) {
        BookingVO bookingVO = new BookingVO();

        bookingVO.setSourceId(booking.getSourceId());

        if (!CollectionUtils.isEmpty(booking.getDetails())) {
            List<BookingVO.BookingDetail> details = Lists.newArrayList();
            for (BookingDO.BookingDetail detail:booking.getDetails()) {
                BookingVO.BookingDetail detailVO = new BookingVO.BookingDetail();
                detailVO.setProductId(detail.getProductId()).setQuantity(detail.getQuantity());
                details.add(detailVO);
            }
            bookingVO.setDetails(details);
        }

        stockService.bookStock(bookingVO);
    }

    @ApiOperation(value = "Change stock quantity", notes = "Change stock quantity")
    @ResponseBody
    @PatchMapping(value = "/{productId}/stock/{quantity}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void changeStockQuantity(@PathVariable String productId, @PathVariable int quantity) {
        stockService.changeStock(productId, quantity);
    }
}
