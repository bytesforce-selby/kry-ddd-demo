package com.kry.inventory.command.application;

import com.kry.exception.OptimisticLockException;
import com.kry.inventory.command.application.valueobjects.*;
import com.kry.inventory.command.domain.stock.ProductId;
import com.kry.inventory.command.domain.stock.Stock;
import com.kry.inventory.command.domain.stock.StockRepository;
import com.kry.inventory.command.domain.stockbooking.BookingDetail;
import com.kry.inventory.command.domain.stockbooking.StockBooking;
import com.kry.inventory.command.domain.stockbooking.StockBookingRepository;
import com.kry.inventory.command.exception.DuplicatedBookingException;
import com.kry.inventory.command.exception.StockNotEnoughException;
import com.kry.utils.KryAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class StockApplicationService {

    @Autowired
    private StockRepository stockRepository;
    @Autowired
    private StockBookingRepository bookingRepository;


    public void bookStock(BookingVO booking) {
        KryAssert.notNull(booking, "booking must be provided.");

        try {
            for (BookingVO.BookingDetail detail : booking.getDetails()) {
                Stock stock = stockRepository.load(detail.getProductId());
                stock.bookFor(booking.getSourceId(), detail.getQuantity());
                stockRepository.save(stock);
            }
        } catch (OptimisticLockException e) {
            throw new StockNotEnoughException();
        }
    }

    public void releaseBooking(BookingExpiredVO bookingExpired) {
        KryAssert.notNull(bookingExpired, "bookingExpired must be provided.");

        Stock stock = stockRepository.load(bookingExpired.getStockId());
        stock.releaseBooking(bookingExpired.getQuantity());
        stockRepository.save(stock);

    }

    public void commitBooking(BookingCompletedVO bookingCompleted) {
        KryAssert.notNull(bookingCompleted, "bookingCompleted must be provided.");

        Stock stock = stockRepository.load(bookingCompleted.getStockId());
        stock.commitBooking(bookingCompleted.getQuantity());
        stockRepository.save(stock);
    }

    public void createStockBooking(BookingCreationVO stockBooking) {
        KryAssert.notNull(stockBooking, "booking must be provided.");

        StockBooking booking = bookingRepository.loadWith(stockBooking.getSourceId(), stockBooking.getProductId());
        if (booking != null) {
            throw new DuplicatedBookingException();
        }
        booking = StockBooking.builder().booking(BookingDetail.builder().sourceId(stockBooking.getSourceId())
                .productId(new ProductId(stockBooking.getProductId()))
                .quantity(stockBooking.getQuantity()).build()).build();
        booking.create();

        bookingRepository.save(booking);
    }

    public void expireBooking(String sourceId) {
        KryAssert.notEmpty(sourceId, "sourceId must be provided.");

        List<StockBooking> bookings = bookingRepository.loadBySource(sourceId);
        for (StockBooking booking: bookings) {
            booking.expire();
            bookingRepository.save(booking);
        }
    }

    public void completeBooking(String sourceId) {
        KryAssert.notEmpty(sourceId, "sourceId must be provided.");

        List<StockBooking> bookings = bookingRepository.loadBySource(sourceId);
        for (StockBooking booking: bookings) {
            booking.complete();
            bookingRepository.save(booking);
        }
    }

    public void createStock(StockCreationVO stockVO) {
        KryAssert.notNull(stockVO, "stockVO must be provided.");

        Stock stock = Stock.builder().productId(new ProductId(stockVO.getProductId()))
                .productName(stockVO.getProductName())
                .stockCnt(stockVO.getQuantity()).build();
        stock.create();

        stockRepository.save(stock);
    }

    public void subtractStock(String stockId, int quantity) {
        KryAssert.notEmpty(stockId, "stockId must be provided.");
        KryAssert.isTrue(quantity > 0, "quantity must be greater than zero.");

        Stock stock = stockRepository.load(stockId);
        stock.subtract(quantity);

        stockRepository.save(stock);
    }

    public void addStock(String stockId, int quantity) {
        KryAssert.notEmpty(stockId, "stockId must be provided.");
        KryAssert.isTrue(quantity > 0, "quantity must be greater than zero.");

        Stock stock = stockRepository.load(stockId);
        stock.add(quantity);

        stockRepository.save(stock);
    }


    public void changeStock(String stockId, int quantity) {
        KryAssert.notEmpty(stockId, "stockId must be provided.");
        KryAssert.isTrue(quantity > 0, "quantity must be greater than zero.");

        Stock stock = stockRepository.load(stockId);
        stock.changeStockQuantity(quantity);

        stockRepository.save(stock);
    }

}
