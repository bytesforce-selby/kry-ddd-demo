package com.kry.inventory.command.infra.repository.stockchange.storageobjects;

import com.kry.infra.repository.BaseSO;
import com.kry.infra.repository.mybatis.annotation.StorageMapper;
import com.kry.inventory.command.infra.repository.stockchange.mybatis.mapper.StockChangeMapper;

import java.io.Serializable;

@StorageMapper(StockChangeMapper.class)
public class StockChangeSO extends BaseSO {


    @Override
    public Serializable getPrimaryKeyValue() {
        return null;
    }
}
