package com.kry.inventory.command.adapter.web.controller.dataobjects;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@ApiModel
@Data
public class BookingDO {

    @ApiModelProperty(value = "Who will book stock, such as Sales Order.", required = true)
    private String sourceId;
    @ApiModelProperty(value = "Booking items.", required = true)
    private List<BookingDetail> details;

    @Data
    public static class BookingDetail {
        @ApiModelProperty(required = true)
        private String productId;
        @ApiModelProperty(required = true)
        private int quantity;
    }
}
