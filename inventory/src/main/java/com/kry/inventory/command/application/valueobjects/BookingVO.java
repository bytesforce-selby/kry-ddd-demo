package com.kry.inventory.command.application.valueobjects;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Accessors(chain = true)
@Data
public class BookingVO {

    @NotNull
    private String sourceId;

    @NotNull
    @NotEmpty
    private List<BookingDetail> details;

    @Data
    public static class BookingDetail {
        @NotNull
        private String productId;
        @NotNull
        private int quantity;
    }

}
